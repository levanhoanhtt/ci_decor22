var app = app || {};

var spBreak = 767;

app.init = function () {

    app.slider_home();
    app.slider_agency();
    app.slider_detail();
    app.slider_other();
    app.tab_js();
    app.matchHeight();
    app.stick_header();
    app.btn_menu();
    app.submenu();
    app.goTop();
    app.list_grid();
    app.language();
    app.stick_side_bar();
    app.swipe();

    app.advancedSearch();
    app.loadMoreProperties();
};

app.isMobile = function () {

    return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;

};

app.isOldIE = function () {

    return $('html.ie9').length || $('html.ie10').length;

};

app.slider_home = function () {
    $('.js-slider-home').slick({
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
        speed: 1500,
        fade: true
    });
};
app.slider_agency = function () {
    $('.js-slider-agents').slick({
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplaySpeed: 3000,
        speed: 800,
        prevArrow: '<a class="slick-prev-circle fa fa-angle-left href="#" ></a>',
        nextArrow: '<a class="slick-next-circle fa fa-angle-right href="#" ></a>',
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
            }
        },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });
};
app.slider_other = function () {
    $('.js-slider-other').slick({
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 3000,
        speed: 800,
        prevArrow: '<a class="slick-prev-circle fa fa-angle-left href="#" ></a>',
        nextArrow: '<a class="slick-next-circle fa fa-angle-right href="#" ></a>',
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            }
        },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });
};

app.slider_detail = function () {
    $('.slider-detail').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<a class="slick-prev-dt href="#" ><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a>',
        nextArrow: '<a class="slick-next-dt href="#" ><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>',
    });
};

app.matchHeight = function () {
    $('.item-matchheight').matchHeight();
};

app.tab_js = function () {
    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
};

app.stick_header = function () {
    $(window).scroll(function (event) {
        var st = $(this).scrollTop();
        if (st > 500) {
            $('.js-main-nav').addClass('sticky');
        } else {
            $('.js-main-nav').removeClass('sticky');
        }
    });
};
app.btn_menu = function () {
    $('.js-btn-menu').click(function () {
        $('.langue-drop').removeClass('active');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.js-main-nav').removeClass('active');
        } else {
            $(this).addClass('active');
            $('.js-main-nav').addClass('active');
        }
        return false;
    });
};
app.submenu = function () {
    $('.item-menu').click(function () {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('active');
        return false;
    });
};
app.goTop = function () {
    $(window).on("scroll load", function () {
        if ($(this).scrollTop() > 100) {
            $('.js-go-top').fadeIn();
        } else {
            $('.js-go-top').fadeOut();
        }
    });
    $('.js-go-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
};
app.list_grid = function () {
    $('.js-view-product').click(function () {
        $('.js-view-product').removeClass('active');
        $(this).addClass('active');
        var value = $(this).attr('data-view');

        if ($('.js-list-item').hasClass(value)) {

        } else {
            $('.js-list-item').removeClass('view-gird').removeClass('view-list');
            $('.js-list-item').addClass(value);
        }

    });
};
app.language = function () {
    $('.js-language').click(function () {
        $(this).next().toggleClass('active');
    });
};

app.stick_side_bar = function () {
    $.stickysidebarscroll("#stick-ban", {
        offset: {
            top: 62,
            bottom: 973,
        }
    });
};

app.swipe = function() {
    window.onload = function() {
        document.addEventListener('swiped-right', function(e) {
            $('.js-btn-menu').removeClass('active');
            $('.js-main-nav').removeClass('active');
        });
    }
};

app.advancedSearch = function(){
    $('.btn-advanced-search').click(function() {
        $('.block-filter-mobile').hide();
        $('.block-filter-desktop').show();
    });
};

app.loadMoreProperties = function(){
    $('#aMoreProperty').click(function(){
        var $this = $(this);
        var pageCurrent = parseInt($('input#pageCurrent').val());
        var pageCount = parseInt($('input#pageCount').val());
        if(pageCurrent < pageCount){
            pageCurrent = pageCurrent + 1;
            var moreTxt = $this.text();
            $this.html(moreTxt + '<i class="fa fa-spinner"></i>');
            $.ajax({
                type: "POST",
                url: $('input#getPropertyUrl').val(),
                data: {
                    Page: pageCurrent,
                    Limit: $('input#limit').val()
                },
                success: function (response) {
                    $this.html(moreTxt);
                    $('#tab-1 > .row').append(response);
                    $('input#pageCurrent').val(pageCurrent);
                    if(pageCurrent == pageCount) $this.remove();
                    app.matchHeight();
                },
                error: function (response) {
                    $this.html(moreTxt);
                }
            });
        }
        return false;
    });
};

$(function () {

    app.init();

});

function formatDecimal(value) {
    value = value.replace(/\,/g, '');
    while (value.length > 1 && value[0] == '0' && value[1] != '.') value = value.substring(1);
    if (value != '' && value != '0') {
        if (value[value.length - 1] != '.') {
            if (value.indexOf('.00') > 0) value = value.substring(0, value.length - 3);
            value = addCommas(value);
            return value;
        }
        else return value;
    }
    else return 0;
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
