$(document).ready(function () {
    $('body').on('focusout', 'input#templateName', function(){
        $('input#templateSlug').val(makeSlug($(this).val()));
    });
    $('#btnUpImage').click(function(){
        $('#inputFileImage').trigger('click');
    });
    $('#inputFileImage').change(function (e) {
        var file;
        for(var i = 0; i < this.files.length; i++) {
            file = this.files[i];
            if(validateImage(file.name)){
                var reader = new FileReader();
                reader.onload = function (f) {
                    $.ajax({
                        type: 'POST',
                        url: $('input#uploadFileUrl').val(),
                        data: {
                            FileBase64: f.target.result,
                            FileTypeId: 3
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            if(json.code == 1) $('#ulProductImages').append('<li><a href="' + json.data + '" target="_blank"><img src="' + json.data + '"></a><i class="fa fa-times"></i></li>');
                            else showNotification(json.message, json.code);
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        }
                    });
                };
                reader.readAsDataURL(file);
            }
        }
    });
    Sortable.create(ulProductImages, {});
    $('#ulProductImages').on('click', 'i', function(){
        $(this).parent().remove();
    });
    $('.submit').click(function(){
        if(validateEmpty()) {
            var templateId = $('input#templateId').val();
            var images = [];
            $('#ulProductImages li a').each(function(){
                images.push($(this).attr('href'));
            });
            $.ajax({
                type: "POST",
                url: $('input#updateTemplateUrl').val(),
                data: {
                    TemplateId: templateId,
                    TemplateName: $('input#templateName').val().trim(),
                    TemplateSlug: $('input#templateSlug').val().trim(),
                    ImageCount: images.length,
                    StatusId: $('select#statusId').val(),
                    Images: JSON.stringify(images)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(templateId == '0') redirect(false, $('a#templateListUrl').attr('href'));
                        else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification($('input#errorCommonMessage').val(), 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
});