$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    // bindCountPromotionUse();
    $('#tbodyPromotion').on('click', '.tdCountPromotionUse a', function(){
        $('#tbodyOrder').html('<tr><td colspan="5" class="text-center">Đang tải đữ liệu <i class="fa fa-spinner"></i></td></tr>');
        $('#modalOrder').modal('show');
        $.ajax({
            type: "POST",
            url: $('input#getListOrderUrl').val(),
            data: {
                PromotionId: $(this).attr('data-id')
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    var data = json.data.ListOrders;
                    var html = '';
                    for (var i = 0; i < data.length; i++){
                        html += '<tr><td class="text-center">' + (i + 1) + '</td>';
                        html += '<td class="text-center">' + data[i].OrderLinkHtml + '</td>';
                        html += '<td class="text-center">' + data[i].CrDateTime + '</td>';
                        html += '<td class="text-center">' + data[i].OrderStatusHtml + '</td>';
                        html += '<td class="text-right">' + data[i].TotalCost + '</td></tr>';
                    }
                    $('#tbodyOrder').html(html);
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('#tbodyOrder').html('');
            }
        });
        return false;
    })
});

function bindCountPromotionUse() {
    var promotionIds = $('input#promotionIds').val().trim();
    if(promotionIds != '' && promotionIds != '[]'){
        $.ajax({
            type: "POST",
            url: $('input#getCountPromotionUseUrl').val(),
            data: {
                PromotionIds: promotionIds
            },
            success: function (response) {
                $('.tdCountPromotionUse').html('0');
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    for(var i = 0; i < data.length; i++) $('tr#promotion_' + data[i].PromotionId).find('.tdCountPromotionUse').html('<a href="javascript:void(0)" data-id="' + data[i].PromotionId + '">' + formatDecimal(data[i].CountPromotion) + '</a>');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }   
}