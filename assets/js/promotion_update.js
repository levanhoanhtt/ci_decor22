$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('#btnGenPromotionCode').click(function(){
        $('input#promotionCode').val(randomPassword(10));
    });
    $('#promotionForm').on('keydown', 'input.cost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input#orderPercent', function () {
        var value = parseInt($(this).val());
        if(value < 0 || value > 100) $(this).val('0');
    }).on('keyup', 'input#orderPaidVN, input#transportCost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    $('input#submit').click(function(){
        if(validateEmpty('#promotionForm')){
            var orderPercent = parseInt($('input#orderPercent').val());
            var orderPaidVN = replaceCost($('input#orderPaidVN').val());
            if(orderPercent == 0 && orderPaidVN == 0){
                showNotification('Vui lòng chọn Số lượng ưu đãi', 0);
                return false;
            }
            var btn = $(this);
            btn.prop('disabled', true);
            var form = $('#promotionForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1 && $('input#promotionId').val() == '0') redirect(false, $('input#promotionIdEditUrl').val() + '/' + json.data);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });
});

function randomPassword(length) {
    //var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var chars = "ABCDEFGHIJKLMNOPQRSTXYZ1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}