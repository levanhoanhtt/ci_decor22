$(document).ready(function(){
	$("body").on('click', '#updateStatus', function(){
		$.ajax({
            type: "POST",
            url: $('input#changeStatusUrl').val(),
            data: {
                OrderId: $("input#orderId").val(),
                OrderStatusId: $("select#orderStatusId").val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra, vui lòng thử lại', 0);
            }
        });
        return false;
	}).on('click', '#btnInsertComment', function(){
        $.ajax({
            type: "POST",
            url: $('input#insertComment').val(),
            data: {
                OrderId: $("input#orderId").val(),
                Comment: $("input#comment").val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra, vui lòng thử lại', 0);
            }
        });
        return false;
    });
    $('.lazyImg').lazyload();
});