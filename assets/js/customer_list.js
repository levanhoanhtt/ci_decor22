$(document).ready(function(){
	$("body").on('click', '.link_delete', function(){
		if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#urlDeleteCustomer').val(),
                data: {
                    CustomerId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) redirect(true, '');
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });	
});