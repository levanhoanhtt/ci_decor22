$(document).ready(function() {
    $("#tbodyArticle").on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')){
            changeStatus($(this).attr('data-id'), '0');
        }
        return false;
    }).on("click", "a.link_status", function(){
        var id = $(this).attr('data-id');
        var statusId = $(this).attr('data-status');
        if(statusId != $('input#statusId_' + id).val()) {
            changeStatus(id, statusId);
        }
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});

function changeStatus(id, statusId) {
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            ArticleId: id,
            StatusId: statusId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1){
                if(id == '0') $('tr#article_' + id).remove();
                else {
                    $('td#statusName_' + id).html(json.data.StatusName);
                    $('input#statusId_' + id).val(statusId);
                }
            }
        },
        error: function (response) {
            showNotification($('input#errorCommonMessage').val(), 0);
        }
    });
}