$(document).ready(function () {
    CKEDITOR.replace('CategoryDesc', {
        language: 'vi',
        toolbar : 'ShortToolbar',
        height: 200
    });
    $('#categoryForm').on('focusout', 'input#categoryName', function(){
        $('input#categorySlug').val(makeSlug($(this).val()));
    });
    $('#btnUpImage').click(function(){
        $('#inputFileImage').trigger('click');
    });
    chooseFile($('#inputFileImage'), $('#fileProgress'), 3, function(fileUrl){
        $('#imgCategory').attr('src', fileUrl).show();
        $('input#categoryImage').val(fileUrl);
    });
    $('.submit').click(function(){
        if(validateEmpty('#categoryForm')) {
            var categoryId = $('input#categoryId').val();
            if(categoryId != '0' && categoryId == $('select#parentCategoryId').val()){
                showNotification('Chuyên mục cha không hợp lệ', 0);
                return false;
            }
            /*for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
             }*/
            $('.submit').prop('disabled', true);
            CKEDITOR.instances['CategoryDesc'].updateElement();
            var form = $('#categoryForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(categoryId == '0') redirect(false, $('a#categoryListUrl').attr('href'));
                        else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification($('input#errorCommonMessage').val(), 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
});