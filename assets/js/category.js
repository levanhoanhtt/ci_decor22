$(document).ready(function() {
    $("#tbodyCategory").on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    CategoryId: id,
                    StatusId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#trItem_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification($('input#errorCommonMessage').val(), 0);
                }
            });
        }
        return false;
    }).on("click", "a.link_status", function(){
        var id = $(this).attr('data-id');
        var statusId = $(this).attr('data-status');
        if(statusId != $('input#statusId_' + id).val()) {
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    CategoryId: id,
                    StatusId: statusId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1){
                        $('td#statusName_' + id).html(json.data);
                        $('input#statusId_' + id).val(statusId);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification($('input#errorCommonMessage').val(), 0);
                }
            });
        }
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});

function changeDisplayOrder(select, categoryId){
    var cateInfo = $('input#cateInfo_' + categoryId);
    $.ajax({
        type: "POST",
        url: $('input#changeDisplayOrderUrl').val(),
        data: {
            CategoryId: categoryId,
            DisplayOrder: select.value,
            ItemTypeId: $('input#itemTypeId').val(),
            CategoryTypeId: cateInfo.attr('data-type'),
            ParentCategoryId: cateInfo.attr('data-parent')
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1) redirect(true, '');
        },
        error: function (response) {
            showNotification($('input#errorCommonMessage').val(), 0);
        }
    });
}