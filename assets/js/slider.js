$(document).ready(function(){
    $('#btnChooseImage').click(function(){
        $('#inputFileImage').trigger('click');
    });
    chooseFile($('#inputFileImage'), $('#fileProgress'), 3, function(fileUrl){
        $('input#sliderImage').val(fileUrl);
        $('#imgSlider').attr('src', fileUrl).show();
    });
    $('#btnChooseImage2').click(function(){
        $('#inputFileImage2').trigger('click');
    });
    chooseFile($('#inputFileImage2'), $('#fileProgress2'), 3, function(fileUrl){
        $('input#sliderIcon').val(fileUrl);
        $('#imgSlider2').attr('src', fileUrl).show();
    });
    $("#tbodySlider").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#sliderId').val(id);
        $('select#displayOrder').val($('select#displayOrder_' + id).val());
        var fileUrl = $('#imgSlider_' + id).attr('src');
        $('input#sliderImage').val(fileUrl);
        $('#imgSlider').attr('src', fileUrl);
        $('#imgSlider').show();
        var fileUrl2 = $('#sliderIcon_' + id).attr('src');
        $('input#sliderIcon').val(fileUrl2);
        $('#imgSlider2').attr('src', fileUrl2);
        $('#imgSlider2').show();
        $('input#sliderLink').val($('td#sliderLink_' + id).text());
        $('input#sliderName').val($('td#sliderName_' + id).text());
        $('input#sliderDesc').val($('td#sliderDesc_' + id).text());
        scrollTo('select#displayOrder');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteSliderUrl').val(),
                data: {
                    SliderId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#slider_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification($('input#errorCommonMessage').val(), 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#sliderForm').trigger("reset");
        $('#imgSlider').hide();
        $('#imgSlider2').hide();
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#sliderImage').val() == ''){
            showNotification('Xin mời chọn ảnh', 0);
            return false;
        }
         if($('input#sliderImage2').val() == ''){
            showNotification('Xin mời chọn icon', 0);
            return false;
        }
        var form = $('#sliderForm');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
            },
            error: function (response) {
                showNotification($('input#errorCommonMessage').val(), 0);
            }
        });
        return false;
    });
});
function changeDisplayOrder(select, sliderId){
    $.ajax({
        type: "POST",
        url: $('input#changeDisplayOrderUrl').val(),
        data: {
            SliderId: sliderId,
            DisplayOrder: select.value,
            SliderTypeId: $('input#sliderTypeId').val()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1) redirect(true, '');
        },
        error: function (response) {
            showNotification($('input#errorCommonMessage').val(), 0);
        }
    });
}