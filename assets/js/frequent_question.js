$(document).ready(function(){
    $("#tbodyFrequentQuestion").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#frequentQuestionId').val(id);
        $('input#questionName').val($('td#questionName_' + id).text());
        $('input#answer').val($('td#answer_' + id).text());
        scrollTo('input#questionName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteFrequentQuestionUrl').val(),
                data: {
                    FrequentQuestionId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#frequentQuestion_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#frequentQuestionForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#frequentQuestionForm')) {
            var form = $('#frequentQuestionForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="frequentQuestion_' + data.FrequentQuestionId + '">';
                            html += '<td id="questionName_' + data.FrequentQuestionId + '">' + data.QuestionName + '</td>';
                            html += '<td id="answer_' + data.FrequentQuestionId + '">' + data.Answer + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.FrequentQuestionId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.FrequentQuestionId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyFrequentQuestion').prepend(html);
                        }
                        else{
                        	$('td#questionName_' + data.FrequentQuestionId).text(data.QuestionName);
                        	$('td#answer_' + data.FrequentQuestionId).text(data.Answer);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});