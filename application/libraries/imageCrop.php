<?php
require APPPATH.'libraries/vendor/autoload.php';

use Intervention\Image\ImageManagerStatic as Image;

class ImageCrop{
    private $image = null;
    private $crop = null;

    public function __construct($imagePath, $cropData, $hasCrop = true){
        $this->image = Image::make($imagePath);
        if($hasCrop) $this->crop = $this->processCropData($cropData);
    }

    private function cropImageBG($image, $width, $height, $x = null, $y = null, $bg_color = null){
        $image_width = $image->width();
        $image_height = $image->height();
        if ($image_width < $width + abs($x) or $x < 0   // Is the crop width outside the image
            or $image_height < $height + abs($y) or $y < 0) {// Is the crop height outside the image
            $canvas_width = abs($x) + $width;
            $canvas_height = abs($y) + $height;
            $background = Image::canvas($canvas_width, $canvas_height);
            if ($bg_color) {
                $background->fill($bg_color);
            }
            $ins_x = abs(($x - abs($x)) / 2);
            $ins_y = abs(($y - abs($y)) / 2);
            $x = abs(($x + abs($x)) / 2);
            $y = abs(($y + abs($y)) / 2);
            $background->insert($image, 'top-left', $ins_x, $ins_y);
            $image = $background;
            unset($background);
        }
        return $image->crop($width, $height, $x, $y);
    }

    public function cropImage($pathSaveTo, $size = 1024, $widthInput = 0, $heightInput = 0){
        try {
            if($this->crop != null) {
                $width = $this->crop->width;
                $height = $this->crop->height;
                $top = $this->crop->top;
                $left = $this->crop->left;
                $sizeCurrent = $this->crop->currentSize;
            }
            else{ // not use
                $width = $widthInput;
                $height = $heightInput;
                $top = 0;
                $left = 0;
                $sizeCurrent = $widthInput > $heightInput ? $heightInput : $widthInput;
            }
            $zSize = floatval($sizeCurrent / $size);
            $zWidth = intval($width / $zSize);
            $zHeight = intval($height / $zSize);
            $zTop = -(intval($top / $zSize));
            $zLeft = -(intval($left / $zSize));
            $this->image = $this->image->resize($zWidth, $zHeight);
            $this->cropImageBG($this->image, $size, $size, $zLeft, $zTop, '#fff')
                ->resizeCanvas(2, 2, 'center', true, '#FEE6F3') //them vien a co
                ->save($pathSaveTo);
            return true;
        }
        catch (Exception $exception) {
            echo $exception->getMessage().PHP_EOL;
            return false;
        }
    }

    private function processCropData($cropData){
        return json_decode(base64_decode($cropData));
    }

    public function getImageSize(){
        return array(
            'width' => $this->image->width(),
            'height' => $this->image->height()
        );
    }
}

