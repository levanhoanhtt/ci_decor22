<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paypal{

    // config sandboxMode = on or sandboxMode = off
    protected $config = array (
        'clientID' => '',
        'sandboxClientID' => 'AZQQHkQ7cNu0RYlv2dorWaxV2BLB2t5ZljcOneAi_Kg8xTmc7_5USBNpK8BdbRM_ZlsPRIqIPCvA-9iY',
        'sandboxMode' => 'on',
        'processUrl' => '/site/paypal'
    );

    public function __construct($params){
        /*$err = array();
        if( !isset($params['amount']) || empty($params['amount']) ){
            $err['amount'] = "amount is require";
        }
        else{
            !is_numeric($params['amount']) ? $err['amount'] = "Amount is numberic" : false ;
        }
        echo ( empty($err) ? $this->init($params['amount'], $params['orderId']) : json_encode($err) );*/
        $this->init($params['amount'], $params['orderId']);
    }


    public function init($amount, $orderId){
        $clientId = $this->config['sandboxMode'] == 'on' ? $this->config['sandboxClientID'] : $this->config['clientID'];
        // load sdk paypal
        ?>
            <style>
                /* Media query for mobile viewport */
                @media screen and (max-width: 400px) {
                    #paypal-button-container {
                        width: 100%;
                    }
                }
                /* Media query for desktop viewport */
                @media screen and (min-width: 400px) {
                    #paypal-button-container {
                        width: 250px;
                    }
                }
            </style>
            <script src="https://www.paypal.com/sdk/js?client-id=<?php echo $clientId ?>"></script>
            <div id="paypal-button"></div>
            <script>
                paypal.Buttons({
                    style: {
                        size: 'small',
                        color: 'gold',
                        shape: 'pill',
                        label: 'checkout',
                    },
                    createOrder: function(data, actions) {
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: "<?php echo $amount; ?>"
                            }
                        }]
                    });
                    },
                    onApprove: function(data, actions) {
                    return actions.order.capture().then(function(details) {
                        return fetch('<?php echo $this->config['processUrl'] ?>', {
                            method: 'post',
                            headers: {
                                'content-type': 'application/json'
                            },
                            body: JSON.stringify({
                                DataPayPal: details,
                                OrderId: "<?php echo $orderId; ?>"
                            })
                        });
                    });
                    },
                    onError: function(error){
                        alert('An error occurred during the execution');
                    }
                }).render('#paypal-button');
            </script>
        <?php
    }
}
?>