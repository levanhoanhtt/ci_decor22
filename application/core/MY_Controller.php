<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class MY_Controller extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(function_exists('date_default_timezone_set')) date_default_timezone_set('Asia/Bangkok');
        //$user = $this->Musers->get(1);$this->session->set_userdata('user', $user);
    }

    protected function commonData($user, $title, $data = array()){
        $data['user'] = $user;
        $data['title'] = $title;
        $data['listActions'] = $this->Mactions->getByUserId($user['UserId']);
        return $data;
    }

    protected function checkUserLogin($isApi = false){
        $user = $this->session->userdata('user');
        if($user){
            $statusId = STATUS_ACTIVED;// $this->Musers->getFieldValue(array('UserId' => $user['UserId']), 'StatusId', 0);
            if($statusId == STATUS_ACTIVED) return $user;
            else{
                $fields = array('user', 'configs');
                foreach($fields as $field) $this->session->unset_userdata($field);
                if($isApi) echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
                else redirect('admin?redirectUrl='.current_url());
                die();
            }
        }
        else{
            if($isApi) echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
            else redirect('admin?redirectUrl='.current_url());
            die();
        }
    }

    protected function loadModel($models = array()){
        foreach($models as $model) $this->load->model($model);
    }

    protected function arrayFromPost($fields) {
        $data = array();
        foreach ($fields as $field) $data[$field] = trim($this->input->post($field));
        return $data;
    }

    protected function arrayFromGet($fields) {
        $data = array();
        foreach ($fields as $field) $data[$field] = trim($this->input->get($field));
        return $data;
    }

    protected function sendMail($emailFrom, $nameFrom, $emailTo, $subject, $messageBody, $files = array(), $emailCC = array()){
        require_once APPPATH.'third_party/swiftmailer/autoload.php';
        $transport = (new Swift_SmtpTransport('smtp.zoho.com', 465, 'ssl'))
            ->setUsername('xinchao@decor22.vn')
            ->setPassword('JHpx.C64q9SaG6u')
        ;
        $mailer = new Swift_Mailer($transport);
        $message = (new Swift_Message($subject))
            ->setFrom([$emailFrom => $nameFrom])
            ->setTo($emailTo)
            ->setBody($messageBody)
            ->setContentType("text/html");
        if(!empty($emailCC)) $message->setCc($emailCC);
        if(!empty($files)) {
            foreach ($files as $file) {
                $message->attach(
                    Swift_Attachment::fromPath($file['url'])->setFilename($file['name'])
                );
            }
        }
        return $mailer->send($message);

    }
}