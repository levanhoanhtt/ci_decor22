<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<?php $this->load->view('includes/breadcrumb'); ?>
		<section class="content">
			<div class="box box-default">
				<?php sectionTitleHtml('Tìm kiếm'); ?>
				<div class="box-body row-margin">
					<?php echo form_open('order'.'/'.$CheckOrderStatusId); ?>
					<div class="row">
                        <div class="col-sm-6">
                            <?php $this->Mconstants->selectObject($listCustomers, 'CustomerId','FullName', 'CustomerId', set_value('CustomerId'), true, 'Khách hàng', ' select2'); ?>
                        </div>
						<div class="col-sm-3">
							<?php $this->Mconstants->selectConstants('orderStatus', 'OrderStatusId', set_value('OrderStatusId'), true, 'Trạng thái'); ?>
						</div>
                    </div>
                    <div class="row">                        
						<div class="col-sm-3">
							<?php $this->Mconstants->selectConstants('totalPhotos', 'TotalPhoto', set_value('TotalPhoto'), true, 'Số lượng ảnh'); ?>
						</div>
                        <div class="col-sm-3">
                            <input type="text" name="RefURL" class="form-control" value="<?php echo set_value('RefURL'); ?>" placeholder="Utm Source">
                        </div>
						<div class="col-sm-3">
							<input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
							<input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="box box-success">
				<?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                <style>.tdCountPromotionUse a{display: block;}</style>
				<div class="box-body no-padding divTable">
					 <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Mã đơn</th>
                                <th>Ngày tạo đơn</th>
                                <?php if($CheckOrderStatusId > 1){ ?>
                                    <th>Ngày đặt hàng</th>
                                <?php } ?>
                                <th>Khách hàng</th>
                                <th class="text-center">TT đơn hàng</th>
                                <th class="text-center">Số lượng ảnh</th>
                                <th class="text-right">Tổng tiền</th>
                                <th>Utm Source</th>
                                <?php if($CheckOrderStatusId > 1){ ?>
                                    <th>Tải về</th>
                                <?php } ?>
                                <th></th>
                            </tr>
                            </thead>
						<tbody id="tbodyOrder">
						<?php $i = 0;
						$status = $this->Mconstants->orderStatus;
						$labelCss = $this->Mconstants->labelCss;
						foreach($listOrders as $p){
							$i++; ?>
							<tr id="order_<?php echo $p['OrderId']; ?>">
								<td><a href="<?php echo base_url('order/view/'.$p['OrderId']); ?>"><?php echo $p['OrderCode']; ?></a></td>
								<td><?php echo ddMMyyyy($p['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                <?php if($CheckOrderStatusId > 1){ ?>
                                    <td><?php echo ddMMyyyy($p['OrderDate'], 'd/m/Y H:i'); ?></td>
                                <?php } ?>
                                <td><?php echo $p['CustomerName']; ?></td>
                                <td class="text-center"><span class="<?php echo $labelCss[$p['OrderStatusId']]; ?>"><?php echo $p['OrderStatus']; ?></span></td>
                                <td class="text-center"><?php echo $p['PhotoCount']; ?></td>
                                <td class="text-right"><?php echo priceFormat($p['PaidVN']); ?></td>
                                <td>
                                    <?php if(!empty($p['RefURL'])){
                                        parse_str(parse_url($p['RefURL'], PHP_URL_QUERY), $urlParam);
                                        if(isset($urlParam['utm_source'])) echo $urlParam['utm_source'];
                                    } ?>
                                </td>
                                <?php if($CheckOrderStatusId > 1){ ?>
                                    <td><a href="<?php echo base_url('order/zipFile').'/'.$p['OrderId']; ?>" target="_bank">Tải về</a></td>
                                <?php } ?>
                                <td>
                                	<?php if($permissionDelete){ ?> 
                                	<a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['OrderId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                	<?php } ?>
                                </td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				<?php $this->load->view('includes/pagging_footer'); ?>
				<?php if($permissionDelete){ ?>
					<input type="text" id="deleteOrderUrl" value="<?php echo base_url('order/delete'); ?>" hidden="hidden">
				<?php } ?>
			</div>
		</section>
	</div>
</div>
<?php $this->load->view('includes/footer'); ?>