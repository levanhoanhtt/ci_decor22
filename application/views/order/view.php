<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php if($orderId > 0){
                $orderStatusId = $order['OrderStatusId']; ?>
            <section class="content-header">
                <h1  class="ttl-list-order ft-seogeo"><span class="title-order"></span>Đơn hàng / <?php echo $order['OrderCode']; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <style>
                    #tbodyOrderPhoto img{width: 300px;max-width: 100%;}
                    .image-main {
                        width: 248px!important;
                        top: 11px!important;
                        position: absolute!important;
                        left: 15px!important;
                        z-index: 9999;
                    }

                    #image-container {
                        position: relative!important;
                        width: 284px!important;
                        left: 0;
                        right: 0;
                        margin: auto;
                    }

                    /*.image-background {
                        width: 100%;
                        height: 100%;
                    }*/

                    .image-main-padding {
                        width: 213px!important;
                        top: 29px!important;
                        position: absolute!important;
                        left: 32px!important;
                        z-index: 9999;
                    }
                </style>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding20">
                            <div class="clearfix top-aded">
                                <div class="left">
                                    <div class="status">
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                        <span id="spanOrderStatus">
                                            <?php echo $this->Mconstants->orderStatus[$orderStatusId]; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
	                                        <tr>
	                                            <th>Loại ảnh</th>
	                                            <th class="text-center">Ảnh gốc</th>
	                                            <th class="text-center">Ảnh chỉnh sửa</th>
	                                        </tr>
                                        </thead>
                                        <tbody id="tbodyOrderPhoto">
                                        	<?php foreach($listOrderPhotos as $op){ ?>
	                                            <tr>
	                                                <td><?php echo $this->Mconstants->photoStyles[$op['PhotoStyleId']]; ?></td>
	                                                <td class="text-center"><img src="<?php echo IMAGE_PATH.$op['OriginalImage']; ?>" class="originalImage lazyImg"></td>
	                                                <td class="text-center">
                                                        <?php if(!empty($op['EditedImage'])){ ?>
                                                            <div id="image-container">
                                                                <img src="<?php echo IMAGE_PATH.$op['EditedImage']; ?>"  class="lazyImg <?php echo in_array($op['PhotoStyleId'], array(1, 3)) ? 'image-main' : 'image-main-padding'; ?>">
                                                                 <img class="image-background" src="<?php echo base_url('assets/icon/'.($op['PhotoStyleId'] > 2 ? 'black.svg' : 'white.svg')); ?>">
                                                            </div>
                                                    <?php } ?>
                                                    </td>
	                                            </tr>
                                        	<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php 
                                if($order['PaidVN'] > 40000){
                                    $orderPhoto = $this->Morderphotos->getTotalPhoto($order['OrderId']); ?>
                                <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
                                	<div class="col-sm-6"></div>
                                	<div class="col-sm-6">
                                		<div class="row tb-sead">
                                            <div class="col-sm-6"><a href="javascript:void(0)" id="aTotalPrice">Tổng tiền</a></div>
                                            <div class="col-sm-6 text-right"><span id="totalPrice"><?php echo priceFormat($orderPhoto['Price']); ?></span> đ</div>
                                        </div>
                                        <hr class="hr-ths">
                                        <div class="row mb10">
                                            <div class="col-sm-6">Giảm giá</div>
                                            <div class="col-sm-6 text-right"><span id=""><?php echo priceFormat($order['Discount']); ?></span> đ</div>
                                        </div>
                                        <hr class="hr-ths">
	                                	<div class="row mb10">
	                                        <div class="col-sm-6">Vận chuyển</div>
	                                        <div class="col-sm-6 text-right"><span id=""><?php echo $order['ShipCost'] > 0 ? priceFormat($order['ShipCost']).'đ': 'Miễn phí'; ?></span> </div>
	                                    </div>
	                                    <div class="row tb-sead">
	                                    	<div class="col-sm-6">Tổng cộng </div>
                                            <div class="col-sm-6 text-right bold light-dark2 fs-16">
                                                <span id="totalCost"><?php echo priceFormat($order['PaidVN']); ?></span> đ
                                            </div>
                                            <!-- <p class="text-right list-btnn">
                                            </p> -->
	                                    </div>
                                	</div>

                                </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <?php if($order['CustomerId'] > 0){ ?>
                            <div class="box box-default mh-wrap-customer" id="divCustomer">
                                <div class="with-border">
                                    <h3 class="box-title">Thông tin khách hàng</h3>
                                    <!-- <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i
                                                class="fa fa-times" style="font-size: 18px;color:#777"></i>
                                        </button>
                                    </div> -->
                                    <div class="mh-info-customer">
                                        <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                        <div class="name-info">
                                            <h4 class="i-name"><?php echo $customer['FullName']; ?></h4>
                                            <div class="phones i-phone"><?php echo $customer['PhoneNumber'] ?></div>
                                        </div>
                                        <div class="total-orders">
                                            <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"><?php echo $totalOrderOfCustomer; ?></span>
                                        </div>
                                       <!--  <div class="bank">
                                            <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance"</span> ₫</span>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div>
                                        <h4 class="mgbt-20 light-blue">Thông tin giao hàng &nbsp;&nbsp;<a href="javascript:void(0)" id="aCustomerAddress"><i class="fa fa-pencil"></i></a></h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="item">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                <span class="i-name"><?php echo $customer['FullName']  ?></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                                <span class="i-phone"><?php echo $customer['PhoneNumber'] ?></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                <span class="i-email"><?php echo  $customer['Email'] ?></span>
                                            </div>
                                            <div class="item i-address">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <span class="i-ward"><span class="spanAddress"><?php echo $customer['Address'] . '</span> ' . $this->Mwards->getWardName($customer['WardId']) ?></span>
                                                <span class="br-line i-district"><?php echo $customer['DistrictId'] > 0 ? $this->Mdistricts->getFieldValue(array('DistrictId' => $customer['DistrictId']), 'DistrictName') : ''; ?></span>
                                                <span class="br-line i-province"><?php echo $customer['ProvinceId'] > 0 ? $this->Mprovinces->getFieldValue(array('ProvinceId' => $customer['ProvinceId']), 'ProvinceName') : ''; ?></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-id-card" aria-hidden="true"></i>
                                                <span class="i-country">
                                                    <?php /*$countryName = $this->Mconstants->getObjectValue($listCountries, 'CountryId', $customer['CountryId'], 'CountryName');
                                                    if(empty($countryName)) */$countryName = 'Việt Nam';
                                                    echo $countryName; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }  ?>
                        <div class="box box-default mh-wrap-customer">
                            <div class="with-border">
                                <h3 class="box-title">Ghi chú</h3>
                                <div class="mh-info-customer">
                                <div class="box-transprt clearfix mb10">
                                    <button type="button" class="btn-updaten save" id="btnInsertComment">Lưu</button>
                                    <input type="text" class="add-text" id="comment" value="<?php echo $order['Comment'] ?>">
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer">
                            <div class="with-border">
                                <h3 class="box-title">Trạng thái đơn hàng</h3>
                                <div class="mh-info-customer">
                                    <div class="row">
                                        <?php if($permissionActiveStatus){ ?>
                                        <div class="col-sm-8">
                                            <?php $this->Mconstants->selectConstants('orderStatus', 'OrderStatusId', $order['OrderStatusId']); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-primary" id="updateStatus">Cập nhật</button>
                                        </div>
                                        <?php }else{ ?>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" disabled value="<?php echo $this->Mconstants->orderStatus[$order['OrderStatusId']]; ?>">
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($urlParam)){
                            $utmFields = array(
                               'utm_source' => 'Utm Source',
                               'utm_medium' => 'Utm Medium',
                               'utm_campaign' => 'Utm Campaign',
                               'utm_content' => 'Utm Content'
                            ); ?>
                            <div class="box box-default mh-wrap-customer" id="divCustomer">
                                <div class="with-border">
                                    <h3 class="box-title">UTM</h3>
                                    <div class="mh-info-customer">
                                        <div class="row">
                                            <?php foreach($utmFields as $uf => $uv){
                                                if(isset($urlParam[$uf])){ ?>
                                                    <div class="col-sm-12">
                                                        <p><b><?php echo $uv ?></b>: <?php echo $urlParam[$uf]; ?></p>
                                                    </div>
                                                <?php }
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if($order['OrderStatusId'] > 1){ ?>
                        <div class="box box-default mh-wrap-customer" id="divCustomer">
                            <div class="with-border">
                                <div class="box-body">
                                    <div class="col-sm-12">
                                        <a href="<?php echo base_url('order/zipFile').'/'.$order['OrderId']; ?>" class="btn btn-primary col-sm-12"  target="_bank">Tải về</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                     <input type="hidden" value="<?php echo base_url('order/changeStatus'); ?>" id="changeStatusUrl">
                     <input type="hidden" value="<?php echo base_url('order/insertComment'); ?>" id="insertComment">
                    <input type="hidden" value="<?php echo $order['OrderId']; ?>" id="orderId">
                </ul>

            </section>
            <?php } else{ ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>