<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('feedback'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="FullName" class="form-control" value="<?php echo set_value('FullName'); ?>" placeholder="Họ và tên">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="PhoneNumber" class="form-control" value="<?php echo set_value('PhoneNumber'); ?>" placeholder="Số điện thoại">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="Email" class="form-control" value="<?php echo set_value('Email'); ?>" placeholder="Email">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Nội dung</th>
                                <th>IpAddress</th>
                                <th>Ngày gửi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            foreach($listFeedbacks as $p){
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $p['FullName']; ?></td>
                                    <td><?php echo $p['PhoneNumber']; ?></td>
                                    <td><?php echo $p['Email']; ?></td>
                                    <td><?php echo $p['Address']; ?></td>
                                    <td><pre class="pre"><?php echo $p['Content']; ?></pre></td>
                                    <td><?php echo $p['IpAddress']; ?></td>
                                   <td><?php echo ddMMyyyy($p['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>