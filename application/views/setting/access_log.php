<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 80px;">STT</th>
                                <th>Time</th>
                                <th>IP</th>
                                <th>Utm Source</th>
                                <th>Utm Medium</th>
                                <th>Utm Campaign</th>
                                <th>Utm Content</th>
                                <th>URL</th>
                                <th>Agent</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            foreach($listLogs as $s){
                                $param = $s['param'];
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $s['time']; ?></td>
                                    <td><?php echo $s['ip']; ?></td>
                                    <td><?php echo isset($param['utm_source']) ? $param['utm_source'] : ''; ?></td>
                                    <td><?php echo isset($param['utm_medium']) ? $param['utm_medium'] : ''; ?></td>
                                    <td><?php echo isset($param['utm_campaign']) ? $param['utm_campaign'] : ''; ?></td>
                                    <td><?php echo isset($param['utm_content']) ? $param['utm_content'] : ''; ?></td>
                                    <td><pre class="pre"><?php echo $s['url']; ?></pre></td>
                                    <td><pre class="pre"><?php echo $s['agent']; ?></pre></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>