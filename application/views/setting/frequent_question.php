<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('frequentquestion/update', array('id' => 'frequentQuestionForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>                                
                                <th>Câu hỏi</th>
                                <th>Câu trả lời</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyFrequentQuestion">
                            <?php
                            foreach($listFrequentQuestions as $s){ ?>
                                <tr id="frequentQuestion_<?php echo $s['FrequentQuestionId']; ?>">
                                    <td id="questionName_<?php echo $s['FrequentQuestionId']; ?>"><?php echo $s['QuestionName']; ?></td>
                                    <td id="answer_<?php echo $s['FrequentQuestionId']; ?>"><?php echo $s['Answer']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $s['FrequentQuestionId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $s['FrequentQuestionId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>                            	
                                <td><input type="text" class="form-control hmdrequired" id="questionName" name="QuestionName" value="" data-field="Câu hỏi"></td>
                                <td><input type="text" class="form-control hmdrequired" id="answer" name="Answer" value="" data-field="Câu trả lời"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="FrequentQuestionId" id="frequentQuestionId" value="0" hidden="hidden">
                                    <input type="text" id="deleteFrequentQuestionUrl" value="<?php echo base_url('frequentquestion/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>