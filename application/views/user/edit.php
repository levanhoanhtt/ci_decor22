<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($canEdit){ ?><li><button class="btn btn-primary submit">Lưu</button></li><?php } ?>
                    <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($userId > 0){ ?>
                    <?php echo form_open('api/user/saveUser', array('id' => 'userForm')); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Họ và tên <span class="required">*</span></label>
                                <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $userEdit['FullName']; ?>" data-field="Họ và tên">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Tên đăng nhập <span class="required">*</span></label>
                                <input type="text" name="UserName" id="userName" class="form-control" value="<?php echo $userEdit['UserName']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Email <span class="required">*</span></label>
                                <input type="text" name="Email" class="form-control hmdrequired" value="<?php echo $userEdit['Email']; ?>" data-field="Email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Di động <span class="required">*</span></label>
                                <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control hmdrequired" value="<?php echo $userEdit['PhoneNumber']; ?>" data-field="Di động">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Ngày sinh <span class="required">*</span></label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control hmdrequired datepicker" name="BirthDay" value="<?php echo ddMMyyyy($userEdit['BirthDay']); ?>" autocomplete="off" data-field="Ngày sinh">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Trạng thái</label>
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', $userEdit['StatusId']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Facebook</label>
                                <input type="text" name="Facebook" class="form-control" value="<?php echo $userEdit['Facebook']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Giới tính</label>
                                <?php $this->Mconstants->selectConstants('genders', 'GenderId', $userEdit['GenderId']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Tỉnh/ Thành phố</label>
                                <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $userEdit['ProvinceId'], false, '', ' select2'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Quận huyện</label>
                                <?php echo $this->Mdistricts->selectHtml($userEdit['DistrictId']); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Phường xã</label>
                                <?php echo $this->Mwards->selectHtml($userEdit['WardId']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Địa chỉ <span class="required">*</span></label>
                                <input type="text" name="Address" class="form-control hmdrequired" value="<?php echo $userEdit['Address']; ?>" data-field="Địa chỉ">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Nhóm quyền <span class="required">*</span></label>
                            <?php $this->Mconstants->selectObject($listGroups, 'GroupId', 'GroupName', 'GroupIds[]', $groupIds, true, '', ' select2', ' multiple'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php $avatar = (empty($userEdit['Avatar']) ? NO_IMAGE : $userEdit['Avatar']); ?>
                                <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 100%;display: block;">
                                <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                            </div>
                            <div class="progress" id="fileProgress" style="display: none;">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <input type="file" style="display: none;" id="inputFileImage">
                            <input type="text" hidden="hidden" id="uploadFileUrl" value="<?php echo base_url('file/upload'); ?>">
                        </div>
                    </div>
                    <?php if($canEdit){ ?>
                        <ul class="list-inline pull-right margin-right-10">
                            <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                            <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Đóng</a></li>
                            <input type="text" name="UserId" id="userId" hidden="hidden" value="<?php echo $userId; ?>">
                            <input type="text" name="UserPass" hidden="hidden" value="<?php echo $userEdit['UserPass']; ?>">
                        </ul>
                    <?php } ?>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>