<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc><?php echo site_url('property-cat-sitemap.xml') ?></loc>
		<lastmod><?php echo date('Y-m-d H:i:s') ?></lastmod>
	</url>
	<url>
		<loc><?php echo site_url('property-sitemap.xml') ?></loc>
		<lastmod><?php echo date('Y-m-d H:i:s') ?></lastmod>
	</url>
</urlset>