<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<?php foreach ($listProperties as $p): ?>
	<url>
		<loc><?php echo $this->Mconstants->getUrl($p['PropertySlug'], $p['PropertyId'], 3); ?></loc>
		<lastmod><?php echo date('Y-m-d H:i:s') ?></lastmod>
	</url>
	<?php endforeach; ?>
</urlset>