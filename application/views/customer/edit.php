<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
        	<?php if ($customerId > 0) { ?>
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Cập nhật</button></li>
                    <li><a href="<?php echo base_url('customer'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('api/customer/update', array('id' => 'customerForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Họ & tên <span class="required">*</span></label>
                            <input type="text" id="fullName" name="FullName" class="form-control hmdrequired" value="<?php echo $customer['FullName']; ?>" data-field="Họ & tên">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Số điện thoại</label>
                            <input type="text" id="phoneNumber" name="PhoneNumber" class="form-control" value="<?php echo $customer['PhoneNumber']; ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Email </label>
                            <input type="text" id="email" name="Email" class="form-control " value="<?php echo $customer['Email']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Giới tính</label>
                            <div class="radio-group">
                                <span class="item"><input type="radio" name="GenderId" class="iCheck" value="1"<?php if($customer['GenderId'] == 1) echo ' checked' ?>> Nam</span>
                                <span class="item"><input type="radio" name="GenderId" class="iCheck" value="2"<?php if($customer['GenderId'] == 2) echo ' checked' ?>> Nữ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Ngày sinh </label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker" id="birthDay" name="BirthDay" value="<?php echo ddMMyyyy($customer['BirthDay']); ?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                    	<div class="form-group">
                            <label class="control-label">Địa chỉ </label>
                            <input type="text" id="address" name="Address" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Quốc gia </label>
                            <?php $this->Mconstants->selectObject($listCountries, 'CountryId', 'CountryName', 'CountryId', $customer['CountryId'], false, '', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Tỉnh / Thành </label>
                            <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $customer['ProvinceId'], true, '--Tỉnh / Thành--', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Quận/ Huyện</label>
                            <?php echo $this->Mdistricts->selectHtml($customer['DistrictId'], 'DistrictId', $listDistricts); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Phường / Xã </label>
                            <?php echo $this->Mwards->selectHtml($customer['WardId']); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Mật khẩu</label>
                             <input type="text" id="password" name="Password" class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Nhắc lại mật khẩu</label>
                            <input type="text" id="rePass" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Cập nhật"></li>
                    <li><a href="<?php echo base_url('customer'); ?>" class="btn btn-default">Đóng</a></li>
                    <input type="text" name="CustomerId" id="customerId" hidden="hidden" value="<?php echo $customerId; ?>">
                    <input type="text" id="editCustomerUrl" hidden="hidden" value="<?php echo base_url('customer/edit'); ?>/">
                </ul>
                <?php echo form_close(); ?>
            </section>
            <?php } else { ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>