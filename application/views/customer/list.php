<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('customer/add'); ?>" class="btn btn-primary">Thên khách hàng</a></li>
                </ul>
            </section>
            <section class="content">
            	<div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                    	<?php echo form_open('customer'); ?>
                    		<div class="row">
                    			<div class="col-sm-10">
                    				<input type="text" class="form-control" name="SearchText" placeholder="Họ, Tên, Họ và tên, Sđt, EMail,....." value="<?php echo set_value('SearchText') ?>">
                    			</div>
                    			<div class="col-sm-2">
                    				<button type="submit" class="btn btn-primary" id="btnSearch">Tìm kiếm</button>
                    			</div>
                    		</div>
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                    	<?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                   <?php $title = '<span class="label label-success"></span> Khách hàng';
                     sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered" >
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ & tên</th>
                                <th>Sđt</th>
                                <th>Email</th>
                                <th>Ngày sinh</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbody">
                                <?php 
                                $i = 0;
                                foreach ($listCustomers as $t) { 
                                    $i++;
                                ?>
                                   <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><a href="<?php echo base_url('customer/edit/'.$t['CustomerId']) ?>"><?php echo $t['FullName']; ?></a></td>
                                        <td><?php echo $t['PhoneNumber']; ?></td>
                                        <td><?php echo $t['Email']; ?></td>
                                        <td><?php echo ddMMyyyy($t['BirthDay']); ?></td>
                                        <td>
                                            <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $t['CustomerId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                   </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <input type="hidden" id="urlDeleteCustomer" value="<?php echo base_url('customer/delete') ?>">
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
