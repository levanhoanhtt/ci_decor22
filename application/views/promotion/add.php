<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php echo form_open('promotion/update', array('id' => 'promotionForm')); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Mã khuyến mại <span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control hmdrequired" name="PromotionCode" id="promotionCode" value="" data-field="Mã khuyến mại">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" id="btnGenPromotionCode">Sinh mã</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Từ ngày <span class="required">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker hmdrequired" name="BeginDate" value="" autocomplete="off" data-field="Từ ngày">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Đến ngày</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker" name="EndDate" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số lần sử dụng chung</label>
                            <input type="text" class="form-control" name="UseLimit" value="0">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số lần sử dụng / Khách hàng</label>
                            <input type="text" class="form-control" name="OrderLimit" value="0">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Trạng thái</label>
                            <?php $this->Mconstants->selectConstants('status', 'StatusId', STATUS_ACTIVED); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số tiền giảm (%)</label>
                            <input type="text" class="form-control cost" name="OrderPercent" id="orderPercent" value="0">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số tiền giảm (VNĐ)</label>
                            <input type="text" class="form-control cost" name="OrderPaidVN" id="orderPaidVN" value="0">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Khách hàng <i>(Không chọn là Tất cả Khách hàng)</i></label>
                    <?php $this->Mconstants->selectObject($listCustomers, 'CustomerId', 'FullName', 'CustomerIds[]', 0, true, '- Chọn khách hàng -', ' select2', ' multiple'); ?>
                </div>
                <div class="form-group text-right">
                    <input class="btn btn-primary" id="submit" type="button" value="Cập nhật">
                    <input type="text" hidden="hidden" name="PromotionId" id="promotionId" value="0">
                    <input type="text" hidden="hidden" id="promotionIdEditUrl" value="<?php echo base_url('promotion/edit'); ?>">
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>