<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<?php $this->load->view('includes/breadcrumb'); ?>
		<section class="content">
			<div class="box box-default">
				<?php sectionTitleHtml('Tìm kiếm'); ?>
				<div class="box-body row-margin">
					<?php echo form_open('promotion'); ?>
					<div class="row">
                        <div class="col-sm-4">
                            <input type="text" name="PromotionCode" class="form-control" value="<?php echo set_value('PromotionCode'); ?>" placeholder="Mã khuyến mại">
                        </div>
                        <div class="col-sm-4">
                            <?php $this->Mconstants->selectObject($listCustomers, 'CustomerId','FullName', 'CustomerId', set_value('CustomerId'), true, 'Khách hàng', ' select2'); ?>
                        </div>
						<div class="col-sm-4">
							<?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'), true, 'Trạng thái'); ?>
						</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                            </div>
                        </div>
						<div class="col-sm-4">
							<input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
							<input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="box box-success">
				<?php sectionTitleHtml('<a href="'.base_url('promotion/add').'" class="btn btn-primary">Thêm mới</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                <?php $promotionIds = array(); ?>
                <style>.tdCountPromotionUse a{display: block;}</style>
				<div class="box-body no-padding divTable">
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th class="text-center alignMiddle" rowspan="2">STT</th>
								<th class="text-center alignMiddle" rowspan="2">Mã khuyến mại</th>
								<th class="text-center alignMiddle" rowspan="2">Từ ngày</th>
								<th class="text-center alignMiddle" rowspan="2">Đến ngày</th>
								<th class="text-center" colspan="3">Tổng số lần sử dụng</th>
								<th class="text-center" colspan="2">Ưu đãi</th>
								<th class="text-center alignMiddle" rowspan="2">Trạng thái</th>
							</tr>
                            <tr>
                                <th class="text-center">Tất cả</th>
                                <th class="text-center">Khách hàng</th>
                                <th class="text-center">Đã sử dụng</th>
                                <th class="text-center">Số tiền giảm (%)</th>
                                <th class="text-center">Số tiền giảm (VNĐ)</th>
                            </tr>
						</thead>
						<tbody id="tbodyPromotion">
						<?php $i = 0;
						$status = $this->Mconstants->status;
						$labelCss = $this->Mconstants->labelCss;
						foreach($listPromotions as $p){
						    $promotionIds[] = $p['PromotionId'];
							$i++; ?>
							<tr id="promotion_<?php echo $p['PromotionId']; ?>">
								<td class="text-center"><?php echo $i; ?></td>
								<td class="text-center"><a href="<?php echo base_url('promotion/edit/'.$p['PromotionId']); ?>"><?php echo $p['PromotionCode']; ?></a></td>
								<td class="text-center"><?php echo ddMMyyyy($p['BeginDate']); ?></td>
								<td class="text-center"><?php echo ddMMyyyy($p['EndDate']); ?></td>
                                <td class="text-center"><?php echo $p['UseLimit']; ?></td>
                                <td class="text-center"><?php echo $p['OrderLimit']; ?></td>
                                <td class="text-center tdCountPromotionUse"><i class="fa fa-spinner"></i></td>
                                <td class="text-center"><?php echo $p['OrderPercent']; ?></td>
                                <td class="text-center"><?php echo priceFormat($p['OrderPaidVN']); ?></td>
                                <td class="text-center"><span class="<?php echo $labelCss[$p['StatusId']]; ?>"><?php echo $status[$p['StatusId']]; ?></span></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				<?php $this->load->view('includes/pagging_footer'); ?>
                <?php // $this->load->view('includes/order/modal_list'); ?>
                <input type="text" hidden="hidden" id="promotionIds" value="<?php echo implode(',', $promotionIds); ?>">
                <input type="text" hidden="hidden" id="getCountPromotionUseUrl" value="<?php echo base_url('promotion/getCountPromotionUse'); ?>">
                <input type="text" id="getListOrderUrl" hidden="hidden" value="<?php echo base_url('order/getList02'); ?>">
			</div>
		</section>
	</div>
</div>
<?php $this->load->view('includes/footer'); ?>