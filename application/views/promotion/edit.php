<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($promotionId > 0){ ?>
                <?php echo form_open('promotion/update', array('id' => 'promotionForm')); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Mã khuyến mại <span class="required">*</span></label>
                            <input type="text" class="form-control hmdrequired" name="PromotionCode" id="promotionCode" value="<?php echo $promotion['PromotionCode']; ?>" data-field="Mã khuyến mại" readonly>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Từ ngày <span class="required">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker hmdrequired" name="BeginDate" value="<?php echo ddMMyyyy($promotion['BeginDate']); ?>" autocomplete="off" data-field="Từ ngày">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Đến ngày</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo ddMMyyyy($promotion['EndDate']); ?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số lần sử dụng chung</label>
                            <input type="text" class="form-control" name="UseLimit" value="<?php echo $promotion['UseLimit']; ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số lần sử dụng / Khách hàng</label>
                            <input type="text" class="form-control" name="OrderLimit" value="<?php echo $promotion['OrderLimit']; ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Trạng thái</label>
                            <?php $this->Mconstants->selectConstants('status', 'StatusId', $promotion['StatusId']); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số tiền giảm (%)</label>
                            <input type="text" class="form-control cost" name="OrderPercent" id="orderPercent" value="<?php echo $promotion['OrderPercent']; ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số tiền giảm (VNĐ)</label>
                            <input type="text" class="form-control cost" name="OrderPaidVN" id="orderPaidVN" value="<?php echo priceFormat($promotion['OrderPaidVN']); ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Khách hàng <i>(Không chọn là Tất cả Khách hàng)</i></label>
                    <?php $this->Mconstants->selectObject($listCustomers, 'CustomerId', 'FullName', 'CustomerIds[]', $customerIds, true, '- Chọn khách hàng -', ' select2', ' multiple'); ?>
                </div>
                <div class="form-group text-right">
                    <input class="btn btn-primary" id="submit" type="button" value="Cập nhật">
                    <input type="text" hidden="hidden" name="PromotionId" id="promotionId" value="<?php echo $promotionId; ?>">
                    <input type="text" hidden="hidden" id="promotionIdEditUrl" value="<?php echo base_url('promotion/edit'); ?>">
                </div>
                <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>