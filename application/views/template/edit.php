<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php if($templateId > 0){ ?>
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Lưu</button></li>
                    <li><a href="<?php echo base_url('template'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding15">
                            <div class="form-group">
                                <label class="control-label normal">Tên <span class="required">*</span></label>
                                <input type="text" name="TemplateName" class="form-control hmdrequired" id="templateName" value="<?php echo $template['TemplateName']; ?>" data-field="Tên">
                            </div>
                            <div class="form-group">
                                <label class="control-label normal">Đường dẫn</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">https://decor22.vn/r/</button>
                                    </div>
                                    <input type="text" name="TemplateSlug" class="form-control" id="templateSlug" value="<?php echo $template['TemplateSlug']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Hình ảnh</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnUpImage"><i class="fa fa-upload"></i> Chọn hình</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <ul class="list-inline" id="ulProductImages" >
                                    <?php foreach($templateImages as $img){ ?>
                                        <li><a href="<?php echo IMAGE_PATH.$img['ImagePath']; ?>" target="_blank"><img  src="<?php echo IMAGE_PATH.$img['ImagePath']; ?>"></a><i class="fa fa-times"></i></li>
                                    <?php } ?>
                                </ul>
                                <div class="progress" id="fileProgress" style="display: none;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <input type="file" multiple="multiple" style="display: none;" id="inputFileImage">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default padding15">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label normal">Trạng thái</label>
                                    <?php $this->Mconstants->selectConstants('status', 'StatusId', $template['StatusId']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                    <li><a href="<?php echo base_url('template'); ?>" id="templateListUrl" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="templateId" name="TemplateId" value="<?php echo $templateId; ?>">
                    <input type="text" hidden="hidden" id="uploadFileUrl" value="<?php echo base_url('file/upload'); ?>">
                    <input type="text" hidden="hidden" id="updateTemplateUrl" value="<?php echo base_url('template/update'); ?>">
                </ul>
            </section>
            <?php } else{ ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>