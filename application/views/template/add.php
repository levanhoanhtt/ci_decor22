<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button">Lưu</button></li>
                    <li><a href="<?php echo base_url('template'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding15">
                            <div class="form-group">
                                <label class="control-label normal">Tên <span class="required">*</span></label>
                                <input type="text" name="TemplateName" class="form-control hmdrequired" id="templateName" value="" data-field="Tên">
                            </div>
                            <div class="form-group">
                                <label class="control-label normal">Đường dẫn</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">https://decor22.vn/r/</button>
                                    </div>
                                    <input type="text" name="TemplateSlug" class="form-control" id="templateSlug" value="">
                                </div>
                            </div>
                        </div>
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Hình ảnh</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnUpImage"><i class="fa fa-upload"></i> Chọn hình</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <ul class="list-inline" id="ulProductImages" ></ul>
                                <div class="progress" id="fileProgress" style="display: none;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <input type="file" multiple="multiple" style="display: none;" id="inputFileImage">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default padding15">
                                <div class="form-group">
                                    <label class="control-label normal">Trạng thái</label>
                                    <?php $this->Mconstants->selectConstants('status', 'StatusId'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="button" value="Lưu"></li>
                    <li><a href="<?php echo base_url('template'); ?>" id="templateListUrl" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="templateId" name="TemplateId" value="0">
                    <input type="text" hidden="hidden" id="uploadFileUrl" value="<?php echo base_url('file/upload'); ?>">
                    <input type="text" hidden="hidden" id="updateTemplateUrl" value="<?php echo base_url('template/update'); ?>">
                </ul>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>