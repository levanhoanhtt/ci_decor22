<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('article/add/'.$articleTypeId); ?>" class="btn btn-default">Thêm mới</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('article/'.$articleTypeId); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" name="ArticleTitle" class="form-control" value="<?php echo set_value('ArticleTitle'); ?>" placeholder="Tên bài viết">
                            </div>
                            <div class="col-sm-3"<?php if($articleTypeId == 2) echo ' style="display: none;"'; ?>>
                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Chọn chuyên mục', ' select2') ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <?php if($articleTypeId == 1) echo '<th>Chuyên mục</th>'; ?>
                                <th>Người viết</th>
                                <th>Ngày tạo</th>
                                <?php if($articleTypeId == 2) echo '<th>Thứ tự</th>'; ?>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyArticle">
                            <?php $i = 0;
                            $status = $this->Mconstants->status;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listArticles as $a){
                                $i++; ?>
                                <tr id="article_<?php echo $a['ArticleId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('article/edit/'.$a['ArticleId']); ?>"><?php echo $a['ArticleTitle']; ?></a></td>
                                    <?php if($articleTypeId == 1){ ?>
                                        <td>
                                            <?php $cateNames = '';
                                            $cateIds = $this->Mcategoryitems->getCateIds($a['ArticleId'], 4);
                                            foreach($cateIds as $cateId) $cateNames .= $this->Mconstants->getObjectValue($listCategories, 'CategoryId', $cateId, 'CategoryName') . ', ';
                                            if(!empty($cateNames)) echo substr($cateNames, 0, strlen($cateNames) - 2); ?>
                                        </td>
                                    <?php } ?>
                                   <td><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $a['CrUserId'], 'FullName'); ?></td>
                                   <td><?php echo ddMMyyyy($a['CrDateTime'], 'd/m/Y'); ?></td>
                                    <?php if($articleTypeId == 2){ ?>
                                        <td><?php echo $a['DisplayOrder']; ?></td>
                                    <?php } ?>
                                    <td id="statusName_<?php echo $a['ArticleId']; ?>"><span class="<?php echo $labelCss[$a['ArticleStatusId']]; ?>"><?php echo $status[$a['ArticleStatusId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $a['ArticleId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $a['ArticleId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $a['ArticleId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $a['ArticleId']; ?>" value="<?php echo $a['ArticleStatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('article/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>