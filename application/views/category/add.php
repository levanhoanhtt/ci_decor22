<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Lưu</button></li>
                    <li><a href="<?php echo base_url('category/index/'.$itemTypeId); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('category/update', array('id' => 'categoryForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding15">
                            <div class="form-group">
                                <label class="control-label normal">Tên Chuyên mục <span class="required">*</span></label>
                                <input type="text" name="CategoryName" class="form-control hmdrequired" id="categoryName" value="" data-field="Tên Chuyên mục">
                            </div>
                            <div class="form-group">
                                <label class="control-label normal">Đường dẫn</label>
                                <input type="text" name="CategorySlug" class="form-control" id="categorySlug" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label normal">Mô tả</label>
                                <textarea name="CategoryDesc" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Phân loại</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label normal">Thứ tự <i>(Thứ tự là 0 sẽ không hiển thị trên web)</i></label>
                                    <?php $this->Mconstants->selectNumber(0, 100, 'DisplayOrder', 1, true); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label normal">Chuyên mục cha</label>
                                    <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'ParentCategoryId', 0, true, '-Chọn chuyên mục-', ' select2') ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label normal">Trạng thái</label>
                                    <?php $this->Mconstants->selectConstants('status', 'StatusId'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ảnh đại diện</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnUpImage"><i class="fa fa-upload"></i> Chọn hình</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <img src="" style="width: 100%;display: none;" id="imgCategory">
                                <div class="progress" id="fileProgress" style="display: none;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <input type="file" style="display: none;" id="inputFileImage">
                                <input type="text" hidden="hidden" id="uploadFileUrl" value="<?php echo base_url('file/upload'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                    <li><a href="<?php echo base_url('category/index/'.$itemTypeId); ?>" id="categoryListUrl" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="categoryId" name="CategoryId" value="0">
                    <input type="text" hidden="hidden" id="categoryImage" name="CategoryImage" value="">
                    <input type="text" hidden="hidden" name="ItemTypeId" value="<?php echo $itemTypeId; ?>">
                    <input type="text" hidden="hidden" name="CategoryTypeId" value="1">
                </ul>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>