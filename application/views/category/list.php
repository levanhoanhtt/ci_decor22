<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('category/add/'.$itemTypeId); ?>" class="btn btn-default">Thêm <?php echo $itemTypeName; ?> mới</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-success">
                    <style>.tdLv1{padding-left: 20px !important;}</style>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">Thứ tự</th>
                                <th>Chuyên mục</th>
                                <th>Slug</th>
                                <th>Chuyên mục cha</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="actions"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCategory">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $status = $this->Mconstants->status;
                            foreach($listCategories as $c){ ?>
                                <tr id="trItem_<?php echo $c['CategoryId']; ?>">
                                    <td>
                                        <?php $attrSelect = ' onchange="changeDisplayOrder(this, \'' . $c['CategoryId'] . '\')" data-id="'.$c['CategoryId'].'"';
                                        $this->Mconstants->selectNumber(1, 100, 'DisplayOrder_'.$c['CategoryId'], $c['DisplayOrder'], true, $attrSelect); ?>
                                    </td>
                                    <td<?php if($c['ParentCategoryId'] > 0) echo ' class="tdLv1"'; ?>><a href="<?php echo base_url('category/edit/'.$c['CategoryId']); ?>"><?php echo $c['CategoryName']; ?></td>
                                    <td><?php echo $c['CategorySlug']; ?> (<a href="<?php echo $this->Mconstants->getUrl($c['CategorySlug'], $c['CategoryId'], $itemTypeId == 3 ? 1 : 5); ?>" target="_blank">Link</a>)</td>
                                    <td><?php if($c['ParentCategoryId'] > 0) echo $this->Mconstants->getObjectValue($listCategories, 'CategoryId', $c['ParentCategoryId'], 'CategoryName'); ?></td>
                                    <td id="statusName_<?php echo $c['CategoryId']; ?>" class="text-center"><span class="<?php echo $labelCss[$c['StatusId']]; ?>"><?php echo $status[$c['StatusId']]; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $c['CategoryId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $c['CategoryId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $c['CategoryId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $c['CategoryId']; ?>" value="<?php echo $c['StatusId']; ?>">
                                        <input type="text" hidden="hidden" id="cateInfo_<?php echo $c['CategoryId']; ?>" value="0" data-type="<?php echo $c['CategoryTypeId']; ?>" data-parent="<?php echo $c['ParentCategoryId']; ?>">
                                    </td>
                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('category/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="changeDisplayOrderUrl" value="<?php echo base_url('category/changeDisplayOrder'); ?>">
                    <input type="text" hidden="hidden" id="itemTypeId" value="<?php echo $itemTypeId; ?>">
                    <input type="text" hidden="hidden" id="itemTypeName" value="<?php echo $itemTypeName; ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>