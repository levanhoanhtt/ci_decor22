<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfeedbacks extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "feedbacks";
        $this->_primary_key = "FeedbackId";
    }
    public function getCount($postData){
        $query = "1 = 1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM feedbacks WHERE 1 = 1" . $this->buildQuery($postData) . ' ORDER BY FeedbackId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['FullName']) && !empty($postData['FullName'])) $query.=" AND FullName LIKE '%{$postData['FullName']}%'";
        if(isset($postData['PhoneNumber']) && !empty($postData['PhoneNumber'])) $query.=" AND PhoneNumber LIKE '%{$postData['PhoneNumber']}%'";
        if(isset($postData['Email']) && !empty($postData['Email'])) $query.=" AND Email LIKE '%{$postData['Email']}%'";
        return $query;
    }
}