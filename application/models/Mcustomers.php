<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customers";
        $this->_primary_key = "CustomerId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM customers WHERE StatusId > 0 " . $this->buildQuery($postData) . ' ORDER BY CustomerId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query .= " AND ( FullName LIKE'%{$postData['SearchText']}%' OR LastName LIKE'%{$postData['SearchText']}%' OR FirstName LIKE'%{$postData['SearchText']}%' OR PhoneNumber LIKE'%{$postData['SearchText']}%' OR Email LIKE'%{$postData['SearchText']}%' OR BirthDay LIKE'%{$postData['SearchText']}%' ) ";
        return $query;
    }

    public function checkExist($customerId, $postData){
        $query = "SELECT CustomerId FROM customers WHERE CustomerId!=? AND StatusId=?";
        $param = array($customerId, STATUS_ACTIVED);
        $conds = '';
        $flag = false;
        $phones = array();
        if(!empty($postData['PhoneNumber'])) $phones[] = $postData['PhoneNumber'];
        if(!empty($postData['Email'])){
            $conds = "Email = ?";
            $param[] = $postData['Email'];
            $flag = true;
        }
        if(!empty($phones)){
            if(!empty($conds)) $conds .= ' OR ';
            $in = '';
            foreach($phones as $phone) $in .= "'{$phone}',";
            $in = substr($in, 0, strlen($in) - 1);
            $conds .= "PhoneNumber IN({$in})";
            $flag = true;
        }
        if($flag) {
            if (!empty($conds)) $query .= " AND ({$conds})";
            $users = $this->getByQuery($query, $param);
            if (!empty($users)) return $users[0]['CustomerId'];
        }
        return 0;
    }

    public function update($postData, $sessionId){
        $id = $this->getFieldValue(array('PhoneNumber' => $postData['PhoneNumber']), 'CustomerId', 0);
        $this->db->trans_begin();
        $customerId = $this->save($postData, $id, array('BirthDay', 'UpdateUserId', 'UpdateDateTime'));
        if($customerId > 0) $this->Morders->updateBy(array('SessionId' => $sessionId), array('CustomerId' => $customerId));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $customerId;
        }
    }
}