<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merrors extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "errors";
        $this->_primary_key = "ErrorId";
    }
}
