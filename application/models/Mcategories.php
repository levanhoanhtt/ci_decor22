<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcategories extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "categories";
        $this->_primary_key = "CategoryId";
    }

    public function update($postData, $categoryId = 0){
        $this->db->trans_begin();
        if($postData['DisplayOrder'] > 0) {
            $this->db->set('DisplayOrder', 'DisplayOrder+1', false);
            $this->db->where(array('ItemTypeId' => $postData['ItemTypeId'], 'CategoryTypeId' => $postData['CategoryTypeId'], 'ParentCategoryId' => $postData['ParentCategoryId'], 'DisplayOrder>=' => $postData['DisplayOrder']));
            $this->db->update('categories');
        }
        $categoryId = $this->save($postData, $categoryId);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $categoryId;
        }
    }

    public function getListByItemType($itemTypeIds, $isAllItemType = false, $isAllStatus = false, $isMerge = false){
        $retVal = array();
        $query = 'SELECT * FROM categories WHERE 1=1 AND StatusId';
        if($isAllStatus) $query .= '> 0';
        else $query .= '='.STATUS_ACTIVED;
        if($isAllItemType) $cates = $this->getByQuery($query . ' ORDER BY DisplayOrder ASC');
        else{
            if(is_numeric($itemTypeIds) && $itemTypeIds > 0) $cates = $this->getByQuery($query. ' AND ItemTypeId = ? ORDER BY DisplayOrder ASC', array($itemTypeIds));
            elseif(is_array($itemTypeIds)) $cates = $this->getByQuery($query . ' AND ItemTypeId IN ? ORDER BY DisplayOrder ASC', array($itemTypeIds));
        }
        if($isMerge){
            foreach ($cates as $c) {
                if ($c['ParentCategoryId'] == 0) {
                    $c['Childs'] = array();
                    foreach ($cates as $c1) {
                        $c1['Childs'] = array();
                        foreach ($cates as $c2) {
                            $c2['Childs'] = array();
                            if ($c2['ParentCategoryId'] == $c1['CategoryId']) $c1['Childs'][] = $c2;
                        }
                        if ($c1['ParentCategoryId'] == $c['CategoryId']) $c['Childs'][] = $c1;
                    }
                    $retVal[] = $c;
                }
            }
        }
        else {
            foreach ($cates as $c) {
                if ($c['ParentCategoryId'] == 0) {
                    $retVal[] = $c;
                    foreach ($cates as $c1) {
                        if ($c1['ParentCategoryId'] == $c['CategoryId']){
                            $retVal[] = $c1;
                            foreach ($cates as $c2) {
                                if ($c2['ParentCategoryId'] == $c1['CategoryId']) $retVal[] = $c2;
                            }
                        }
                    }
                }
            }
        }
        return $retVal;
    }

    public function getListByItem($itemId, $itemTypeId, $categoryTypeId = 1){
        return $this->getByQuery('SELECT CategoryId, CategoryName, CategorySlug FROM categories WHERE ItemTypeId = ? AND CategoryId IN(SELECT CategoryId FROM categoryitems WHERE ItemId = ? AND ItemTypeId = ?)', array($categoryTypeId, $itemId, $itemTypeId));
    }

    public function getItemTypes($cateIds){
        $retVal = array();
        $cates = $this->getByQuery('SELECT CategoryId, ItemTypeId FROM categories WHERE StatusId = ? AND CategoryId IN ?', array(STATUS_ACTIVED, $cateIds));
        foreach($cates as $c) $retVal[$c['CategoryId']] = $c['ItemTypeId'];
        return $retVal;
    }

    public function getCategoryNames($cateIds){
        $retVal = array();
        $cates = $this->getByQuery('SELECT CategoryName FROM categories WHERE StatusId = ? AND CategoryId IN ?', array(STATUS_ACTIVED, $cateIds));
        foreach($cates as $c) $retVal[] = $c['CategoryName'];
        return $retVal;
    }
}
