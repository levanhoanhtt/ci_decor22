<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomerpromotions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customerpromotions";
        $this->_primary_key = "CustomerPromotionId";
    }
}