<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpromotions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "promotions";
        $this->_primary_key = "PromotionId";
    }

    public function checkExist($promotionId, $promotionCode){
        $promotions = $this->getByQuery('SELECT PromotionId FROM promotions WHERE PromotionId != ? AND PromotionCode = ? AND StatusId > 0 LIMIT 1', array($promotionId, $promotionCode));
        return !empty($promotions);
    }

    public function checkPromotion($customerId, $promotion = false, $promotionId = 0){
        $flag = false;
        if(!$promotion && $promotionId > 0) $promotion = $this->get($promotionId);
        if($promotion) {
            $flag = true;
            if(!empty($promotion['EndDate'])) {
                $today = strtotime(date("Y-m-d"));
                $endDate = strtotime($promotion['EndDate'] . ' 23:59:59');
                $flag = $endDate >= $today;
            }
            if($flag && $promotion['IsAllCustomer'] == 0) {
                $this->load->model('Mcustomerpromotions');
                $customerPromotionId = $this->Mcustomerpromotions->getFieldValue(array('PromotionId' => $promotion['PromotionId'], 'CustomerId' => $customerId), 'CustomerPromotionId', 0);
                $flag = $customerPromotionId > 0;
            }
            if ($flag && ($promotion['UseLimit'] > 0 || $promotion['OrderLimit'] > 0)) {
                $this->load->model('Morderpromotions');
                $listOrderPromotions = $this->Morderpromotions->getBy(array('PromotionId' => $promotion['PromotionId'], 'StatusId' => 2));
                if ($promotion['UseLimit'] > 0) $flag = $promotion['UseLimit'] > count($listOrderPromotions);
                if ($flag && $promotion['OrderLimit'] > 0) {
                    $orderLimit = 0;
                    foreach ($listOrderPromotions as $op) {
                        if ($op['CustomerId'] == $customerId) $orderLimit++;
                    }
                    $flag = $promotion['OrderLimit'] > $orderLimit;
                }
            }
        }
        return $flag;
    }

    public function checkPromotionCode($sessionId, $promotionCode){
        return false;
        /*$this->load->model('Mcustomers');
        $this->load->model('Mcustomerpromotions');
        $flag = false;
        $customerId = $this->Mcustomers->getFieldValue(array('SessionId' => $sessionId, 'StatusId' => STATUS_ACTIVED), 'CustomerId', 0);
        if($customerId){
            $promotions = $this->getByQuery('SELECT * FROM promotions WHERE PromotionCode = ? AND StatusId > 0 AND UseLimit > 0 AND OrderLimit > 0 AND BeginDate <= CURRENT_DATE()  AND (EndDate >= CURRENT_DATE() OR EndDate IS NULL) LIMIT 1', array($promotionCode));
            if($promotions){
                // Tính số lần sử dụng mã cho 1 id khách
                $promotionId = $promotions[0]['PromotionId'];
                $customerpromotion = $this->getByQuery('SELECT customerpromotions.* FROM `orders` INNER JOIN customerpromotions ON customerpromotions.OrderId = orders.OrderId WHERE orders.CustomerId = ? AND orders.SessionId = ? AND customerpromotions.PromotionId = ? LIMIT 1', array($customerId, $sessionId, $promotionId));
                if(count($customerpromotion) == 1 AND ( count($customerpromotion) < $promotions[0]['OrderLimit'])){
                    $flag = $this->Mcustomerpromotions->save(array('CustomerId' => $customerId, 'PromotionId' => $promotionId));
                    if($flag){
                        $flag = true;
                        $this->db->query('UPDATE promotions SET UseLimit = UseLimit - 1 WHERE PromotionId = ?', array($promotionId));
                    }
                }
            }
        }
        return $flag;*/
    }

    public function getPercent($order){
        return $this->getByQuery("SELECT promotions.OrderPaidVN AS OrderPaidVN, promotions.OrderPercent AS OrderPercent FROM `customerpromotions` INNER JOIN promotions ON promotions.PromotionId = customerpromotions.PromotionId WHERE customerpromotions.CustomerId = ? AND customerpromotions.OrderId = ? ", array($order['CustomerId'], $order['OrderId']));
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $select = '*'){
        $query = "SELECT {$select} FROM promotions WHERE StatusId > 0" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['PromotionCode']) && !empty($postData['PromotionCode'])) $query.=" AND PromotionCode = '{$postData['PromotionCode']}'";
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId = ".$postData['StatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND DATE(BeginDate) >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND DATE(EndDate) <= '{$postData['EndDate']}'";
        if(isset($postData['CustomerId'])){
            if($postData['CustomerId'] > 0) $query.=" AND PromotionId IN(SELECT PromotionId FROM customerpromotions WHERE CustomerId = {$postData['CustomerId']})";
            elseif($postData['CustomerId'] == -1) $query.=" AND IsAllCustomer = 1";
        }
        return $query;
    }

    public function getListUse($customerId){
        return $this->getByQuery('SELECT PromotionId, PromotionCode FROM promotions WHERE StatusId > 0 AND PromotionId IN(SELECT PromotionId FROM orderpromotions WHERE CustomerId = ? AND StatusId = ?)', array($customerId, 2));
    }

    public function update($postData, $promotionId = 0, $customerIds = array()){
        $this->db->trans_begin();
        if($promotionId > 0) $this->db->delete('customerpromotions', array('PromotionId' => $promotionId));
        $promotionId = $this->save($postData, $promotionId, array('EndDate', 'UpdateUserId', 'UpdateDateTime'));
        if(!empty($customerIds)){
            $customerPromotions = array();
            foreach($customerIds as $customerId){
                $customerPromotions[] = array(
                    'CustomerId' => $customerId,
                    'PromotionId' => $promotionId
                );
            }
            $this->db->insert_batch('customerpromotions', $customerPromotions);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $promotionId;
        }
    }
}
