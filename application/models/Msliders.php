<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msliders extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "sliders";
        $this->_primary_key = "SliderId";
    }

    public function getList($sliderTypeId){
        return $this->getBy(array('SliderTypeId' => $sliderTypeId), false, 'DisplayOrder', '', 0, 0, 'asc');
    }

    public function getApiList($sliderTypeId){
        $datas = $this->getBy(array('SliderTypeId' => $sliderTypeId), false, 'DisplayOrder', '', 0, 0, 'asc');
        $arrDatas = array();
        foreach ($datas as $d) {
            $arrDatas[] = array(
                'imageUrl' => base_url().IMAGE_PATH.$d['SliderImage'],
                'sourceIconUrl' => base_url().IMAGE_PATH.$d['SliderIcon'],
                'authorName' => $d['SliderNickname'],
                'comment' => $d['SliderDesc']
            );
        }
        return $arrDatas;
    }

    public function update($postData, $sliderId){
        $this->db->trans_begin();
        $this->db->set('DisplayOrder', 'DisplayOrder+1', false);
        $this->db->where(array('SliderTypeId' => $postData['SliderTypeId'], 'DisplayOrder>=' => $postData['DisplayOrder']));
        $this->db->update('sliders');
        $sliderId = $this->save($postData, $sliderId);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $sliderId;
        }
    }
}
