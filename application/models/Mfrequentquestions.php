<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfrequentquestions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "frequentquestions";
        $this->_primary_key = "FrequentQuestionId";
    }
}