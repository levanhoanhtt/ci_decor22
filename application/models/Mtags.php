<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtags extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "tags";
        $this->_primary_key = "TagId";
    }

    public function getTagId($tagName, $itemTypeId){
        $tagId = $this->getFieldValue(array('TagName' => $tagName, 'ItemTypeId' => $itemTypeId), 'TagId', 0);
        if($tagId == 0) $tagId = $this->save(array('TagName' => $tagName, 'TagSlug' => makeSlug($tagName), 'ItemTypeId' => $itemTypeId), 0);
        return $tagId;
    }

    public function getTagNames($itemId, $itemTypeId){
        $retVal = array();
        $tags = $this->getByQuery('SELECT TagName FROM tags WHERE TagId IN(SELECT TagId FROM itemtags WHERE ItemId = ? AND ItemTypeId = ?)', array($itemId, $itemTypeId));
        foreach($tags as $tag) $retVal[] = $tag['TagName'];
        return $retVal;
    }

    public function getCommonTagName($itemTypeId, $limit = 0){
        $retVal = array();
        $query = 'SELECT TagName, COUNT(ItemTagId) as CountItem FROM tags INNER JOIN itemtags ON tags.TagId = itemtags.TagId WHERE tags.ItemTypeId = ? GROUP BY TagName ORDER BY CountItem DESC';
        if($limit > 0) $query .= ' LIMIT '.$limit;
        $tags = $this->getByQuery($query, array($itemTypeId));
        foreach($tags as $tag) $retVal[] = $tag['TagName'];
        return $retVal;
    }

    public function updateItem($itemIds, $tagNames, $itemTypeId, $changeTagTypeId){
        $this->load->model('Mitemtags');
        $this->db->trans_begin();
        $tagIds = array();
        foreach($tagNames as $tagName) $tagIds[] = $this->getTagId($tagName, $itemTypeId);
        if(!empty($tagIds)){
            if ($changeTagTypeId == 1) { //add
                foreach($itemIds as $itemId) {
                    foreach ($tagIds as $tagId) $this->Mitemtags->update(array('ItemId' => $itemId, 'ItemTypeId' => $itemTypeId, 'TagId' => $tagId));
                }
            }
            elseif ($changeTagTypeId == 2) { //remove

                $this->db->query('DELETE FROM itemtags WHERE ItemTypeId = ? AND ItemId IN ? AND TagId IN ?', array($itemTypeId, $itemIds, $tagIds));
            }
            elseif ($changeTagTypeId == 3){ //update
                $this->db->query('DELETE FROM itemtags WHERE ItemTypeId = ? AND ItemId IN ?', array($itemTypeId, $itemIds));
                $itemTags = array();
                foreach($itemIds as $itemId) {
                    foreach ($tagIds as $tagId){
                        $itemTags[] = array(
                            'ItemId' => $itemId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if (!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getCount($postData){
        $query = "TagId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM tags WHERE TagId > 0" . $this->buildQuery($postData) . ' ORDER BY TagId ASC';
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if (isset($postData['ItemTypeId']) && $postData['ItemTypeId'] > 0) $query .= " AND ItemTypeId=".$postData['ItemTypeId'];
        return $query;
    }
}
