<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconstants extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /*public $articleTypes = array(
        1 => 'Post',
        2 => 'Page'
    );*/

    public $menuPositions = array(
        1 => 'Cuối trang chủ',
        2 => 'Đầu trang',
    );

    public $photoStyles = array(
        1 => 'Trắng',//'Clean',
        2 => 'Trắng viền',
        3 => 'Đen',// 'Bold',
        4 => 'Đen viền'
    );

    public $totalPhotos = array(
        '> 0' => '> 0',
        '<= 0' => '<= 0'
    );

    //
    public $itemStatus = array(
        2 => 'Đang hoạt động',
        1 => 'Tạm dừng',
        3 => 'Dừng hoạt động'
    );

    public $status = array(
        2 => 'Đã duyệt',
        1 => 'Chưa duyệt',
        3 => 'Không được duyệt',
        4 => 'Xem xét thêm'
    );

    public $genders = array(
        1 => 'Nam',
        2 => 'Nữ'
    );

    public $orderStatus = array(
        1 => 'Nháp',
        2 => 'Chờ xử lý',
        3 => 'Đang xử lý', // => Gửi VC'
        4 => 'Thành công',
    );

    /*public $itemTypes = array(
        4 => 'Bài viết'
    );*/

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-success',
        3 => 'label label-warning',
        4 => 'label label-danger',
        5 => 'label label-default',
        6 => 'label label-success',
        7 => 'label label-warning',
        8 => 'label label-danger',
        9 => 'label label-default',
        10 => 'label label-success',
        11 => 'label label-warning',
        12 => 'label label-danger'
    );

    public function selectConstants($key, $selectName, $itemId = 0, $isAll = false, $txtAll = 'Tất cả', $selectClass = '', $attrSelect = ''){
        $obj = $this->$key;
        if($obj) {
            echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
            if($isAll) echo '<option value="0">'.$txtAll.'</option>';
            foreach($obj as $i => $v){
                if($itemId == $i) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$v.'</option>';
            }
            echo "</select>";
        }
    }

    public function selectObject($listObj, $objKey, $objValue, $selectName, $objId = 0, $isAll = false, $txtAll = "Tất cả", $selectClass = '', $attrSelect = ''){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.$id.'"'.$attrSelect.'>';
        if($isAll) echo '<option value="0">'.$txtAll.'</option>';
        /*if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }*/
        $isSelectMutiple = is_array($objId);
        foreach($listObj as $obj){
            $selected = '';
            if(!$isSelectMutiple) {
                if ($obj[$objKey] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj[$objKey], $objId)) $selected = ' selected="selected"';
            echo '<option value="'.$obj[$objKey].'"'.$selected.'>'.$obj[$objValue].'</option>';
        }
        echo '</select>';
    }

    public function selectNumber($start, $end, $selectName, $itemId = 0, $asc = false, $attrSelect = ''){
        echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
        if($asc){
            for($i = $start; $i <= $end; $i++){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        else{
            for($i = $end; $i >= $start; $i--){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        echo '</select>';
    }

    public function getObjectValue($listObj, $objKey, $objValue, $objKeyReturn, $returnObj = false){
        foreach($listObj as $obj){
            if($obj[$objKey] == $objValue){
                if($returnObj) return $obj;
                return $obj[$objKeyReturn];
            }
        }
        return $returnObj ? false : '';
    }

    public function getUrl($itemSlug, $itemId, $itemTypeId){
        $retVal = 'javascript:void(0)';
        if ($itemTypeId == 1) $retVal = base_url('category/' . $itemSlug . '-c' . $itemId . '.html');
        elseif ($itemTypeId == 4) $retVal = base_url('article/' . $itemSlug . '-a' . $itemId . '.html');
        elseif ($itemTypeId == 5) $retVal = base_url('news/' . $itemSlug . '-c' . $itemId . '.html');
        return $retVal;
    }
}