<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtemplates extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table_name = "templates";
        $this->_primary_key = "TemplateId";
    }

    public function checkExist($templateSlug, $templateId = 0){
        $query = "SELECT TemplateId FROM templates WHERE TemplateId != ? AND StatusId > 0 AND TemplateSlug = ?";
        $templates = $this->getByQuery($query, array($templateId, $templateSlug));
        if (!empty($templates)) return true;
        return false;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM templates WHERE StatusId > 0" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['TemplateName']) && !empty($postData['TemplateName'])) $query.=" AND TemplateName LIKE '%{$postData['TemplateName']}%'";
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0)  $query.=" AND StatusId=".$postData['StatusId'];
        return $query;
    }

    public function update($postData, $templateId, $images = array()){
        $isUpdate = $templateId > 0; 
        $this->db->trans_begin();
        $templateId = $this->save($postData, $templateId);
        if($templateId > 0){
            if($isUpdate) $this->db->delete('templateimages', array('TemplateId' => $templateId));
            if(!empty($images)){
                require_once APPPATH . "/libraries/imageCrop.php";
                $templateImages = array();
                foreach ($images as $image){
                    $imageCrop = new ImageCrop(IMAGE_PATH.$image, '', false);
                    $size = $imageCrop->getImageSize();
                    $templateImages[] = array(
                        'TemplateId' => $templateId,
                        'ImagePath' => $image,
                        'Width' => $size['width'],
                        'Height' => $size['height']
                    );
                }
                if(!empty($templateImages)) $this->db->insert_batch('templateimages', $templateImages);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $templateId;
        }
    }
}