<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtemplateimages extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table_name = "templateimages";
        $this->_primary_key = "UserGroupId";
    }
}