<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morderphotos extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orderphotos";
        $this->_primary_key = "OrderPhotoId";
    }

    public function getTotalPhoto($orderId){
    	$data =  $this->getByQuery("SELECT SUM(Price) AS Price, COUNT(OrderId) AS TotalPhoto FROM orderphotos WHERE OrderId = ? AND StatusId > ?", array($orderId, 0));
    	if($data) return $data[0];
    	return false;
    }

    public function removeCheck($ids, $orderId){
        $this->db->query('UPDATE orderphotos SET StatusId = 0 WHERE OrderId = ? AND OrderPhotoId NOT IN ?', array($orderId, $ids));
    }

    public function getIdByImage($image){
        $ops = $this->getByQuery('SELECT OrderPhotoId FROM orderphotos WHERE OriginalImage = ? OR EditedImage = ? LIMIT 1', array($image, $image));
        if(!empty($ops)) return $ops[0]['OrderPhotoId'];
        return 0;
    }

    public function getCountPhoto($orderIds){
        $retVal = array();
        $ops = $this->getByQuery('SELECT OrderId, COUNT(OrderPhotoId) AS CountPhoto FROM orderphotos WHERE StatusId > 0 AND OrderId IN ? GROUP BY OrderId', array($orderIds));
        foreach ($ops as $op) $retVal[$op['OrderId']] = $op['CountPhoto'];
        return $retVal;
    }
}