<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morders extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orders";
        $this->_primary_key = "OrderId";
    }

    public function getCount($postData){
        $query = "OrderStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $orderBy = 'OrderId'){
        $query = "SELECT * FROM orders WHERE OrderStatusId > 0 " . $this->buildQuery($postData) . " ORDER BY {$orderBy} DESC";
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['CheckOrderStatusId']) && $postData['CheckOrderStatusId'] == 1)  $query.=" AND OrderStatusId = '{$postData['CheckOrderStatusId']}'";
        else $query.=" AND OrderStatusId > 1";
        if(isset($postData['CustomerId']) && !empty($postData['CustomerId'])) $query.=" AND CustomerId = ".$postData['CustomerId'];
        if(isset($postData['OrderStatusId']) && !empty($postData['OrderStatusId'])) $query.=" AND OrderStatusId = '{$postData['OrderStatusId']}'";
        if(isset($postData['TotalPhoto']) && !empty($postData['TotalPhoto'])) $query.=" AND PhotoCount ".$postData['TotalPhoto'];
        if(isset($postData['RefURL']) && !empty($postData['RefURL'])) $query.=" AND RefURL LIKE '%{$postData['RefURL']}%'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND DATE(CrDateTime) >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND DATE(CrDateTime) <= '{$postData['EndDate']}'";
        return $query;
    }

    public function genOrderCode($orderId, $text = 'DH-'){
        return $text.($orderId);
    }

    public function updateCheckout($photoUpdates, $sessionId){
        $this->db->trans_begin();
        $this->db->update_batch('orderphotos', $photoUpdates, 'OrderPhotoId');
        $this->updateBy(array('SessionId' => $sessionId), array('OrderStatusId' => 2, 'OrderDate' => getCurentDateTime()));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getOrderInImages(){
        $query = "SELECT orders.*, (SELECT count(orderphotos.OrderId) FROM orderphotos where orderphotos.StatusId = ? AND orders.OrderId = orderphotos.OrderId ) as QuantityImage FROM orders WHERE OrderStatusId >= ? AND IsSendMail = ? ORDER BY OrderId DESC";
        return $this->getByQuery($query, array(STATUS_ACTIVED, STATUS_ACTIVED, 0));
    }
    
    public function getPhotoCount(){
        $retVal = array();
        $orders = $this->getByQuery("SELECT OrderId, COUNT(OrderPhotoId) AS CountPhoto FROM orderphotos WHERE StatusId = 2 AND OrderId IN(SELECT OrderId FROM orders WHERE CrDateTime > now() - interval 1 day) GROUP BY OrderId");
        foreach($orders as $o) $retVal[$o['OrderId']] = $o['CountPhoto'];
        return $retVal;
    }
}