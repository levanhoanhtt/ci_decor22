<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Đơn mẫu',
            array('scriptFooter' => array('js' => 'js/template.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'template')) {
            $this->loadModel(array('Mtemplates'));
            $postData = $this->arrayFromPost(array('TemplateName', 'StatusId'));
            $rowCount = $this->Mtemplates->getCount($postData);
            $data['listTemplates'] = array();
            if($rowCount > 0){
                $perPage = 20;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listTemplates'] = $this->Mtemplates->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('template/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm mới Đơn mẫu',
            array('scriptFooter' => array('js' => array('vendor/plugins/sortable/Sortable.min.js', 'js/template_update.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'template')) {
            $this->load->view('template/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($templateId){
        $user = $this->checkUserLogin();
        if($templateId > 0){
            $data = $this->commonData($user,
                'Cật nhật Đơn mẫu',
                array('scriptFooter' => array('js' => array('vendor/plugins/sortable/Sortable.min.js', 'js/template_update.js')))
            );
            if($this->Mactions->checkAccess($data['listActions'], 'template')) {
                $this->loadModel(array('Mtemplates', 'Mtemplateimages'));
                $template = $this->Mtemplates->get($templateId);
                if($template) {
                    $data['templateId'] = $templateId;
                    $data['template'] = $template;
                    $data['templateImages'] = $this->Mtemplateimages->getBy(array('TemplateId' => $templateId));
                    $this->load->view('template/edit', $data);
                }
                else {
                    $data['templateId'] = 0;
                    $data['txtError'] = "Không tìm thấy Đơn mẫu";
                }
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('template');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TemplateName', 'TemplateSlug', 'ImageCount', 'StatusId'));
        if(!empty($postData['TemplateName']) && $postData['StatusId'] > 0) {
            if (empty($postData['TemplateSlug'])) $postData['TemplateSlug'] = makeSlug($postData['TemplateName']);
            else $postData['TemplateSlug'] = makeSlug($postData['TemplateSlug']);
            $this->load->model('Mtemplates');
            $templateId = $this->input->post('TemplateId');
            if($this->Mtemplates->checkExist($postData['TemplateSlug'], $templateId)){
                echo json_encode(array('code' => 0, 'message' => 'Đương dẫn đã tồn tại'));
                die();
            }
            $crDateTime = getCurentDateTime();
            if ($templateId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
            }

            $images = json_decode(trim(replaceFileUrl($this->input->post('Images'))), true);
            $templateId = $this->Mtemplates->update($postData, $templateId, $images);
            if ($templateId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Đơn mẫu thành công", 'data' => $templateId));
            else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
        }
        else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $templateId = $this->input->post('TemplateId');
        $statusId = $this->input->post('StatusId');
        if($templateId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
            $this->load->model('Mtemplates');
            $flag = $this->Mtemplates->changeStatus($statusId, $templateId, 'StatusId', $user['UserId']);
            if($flag) {
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa Đơn mẫu thành công";
                else{
                    $txtSuccess = "Đổi trạng thái thành công";
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
        }
        else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
    }
}