<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends MY_Controller {

	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Mã khuyến mại',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/promotion_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'promotion')) {
            $this->loadModel(array('Mcustomers', 'Mpromotions'));
            $listCustomers = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
            array_unshift($listCustomers,array('CustomerId' => -1, 'StatusId' => STATUS_ACTIVED, 'FullName' => 'Tất cả Khách hàng', 'PhoneNumber' => ''));
            $data['listCustomers'] = $listCustomers;
            $postData = $this->arrayFromPost(array('PromotionCode', 'CustomerId', 'StatusId', 'BeginDate', 'EndDate'));
            if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
            if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
            $rowCount = $this->Mpromotions->getCount($postData);
            $data['listPromotions'] = array();
            if($rowCount > 0){
                $pageCount = ceil($rowCount / DEFAULT_LIMIT);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listPromotions'] = $this->Mpromotions->search($postData, DEFAULT_LIMIT, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('promotion/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Mã khuyến mại',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/promotion_update.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'promotion/add')) {
            $this->load->model('Mcustomers');
            $data['listCustomers'] = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('promotion/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($promotionId = 0){
	    if($promotionId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Sửa Mã khuyến mại',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/promotion_update.js'))
                )
            );
            if($this->Mactions->checkAccess($data['listActions'], 'promotion/edit')) {
                $this->loadModel(array('Mpromotions', 'Mcustomerpromotions', 'Mcustomers'));
                $promotion = $this->Mpromotions->get($promotionId);
                if($promotion && $promotion['StatusId'] > 0){
                    $data['promotionId'] = $promotionId;
                    $data['promotion'] = $promotion;
                    $data['listCustomers'] = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['customerIds'] = $this->Mcustomerpromotions->getListFieldValue(array('PromotionId' => $promotionId), 'CustomerId');
                }
                else {
                    $data['promotionId'] = 0;
                    $data['txtError'] = "Không tìm thấy Mã khuyến mại";
                }
                $this->load->view('promotion/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
	    else redirect('promotion');
    }

    public function update(){
	    $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('PromotionCode', 'BeginDate', 'EndDate', 'UseLimit', 'OrderLimit', 'StatusId', 'OrderPercent', 'OrderPaidVN'));
        $postData['OrderPaidVN'] = replacePrice($postData['OrderPaidVN']);
        if(!empty($postData['PromotionCode']) && !empty($postData['BeginDate']) && $postData['StatusId'] > 0 && ($postData['OrderPercent'] > 0 || $postData['OrderPaidVN'] > 0)){
            $this->load->model('Mpromotions');
            $promotionId = $this->input->post('PromotionId');
            if($this->Mpromotions->checkExist($promotionId, $postData['PromotionCode'])){
                echo json_encode(array('code' => -1, 'message' => "Mã khuyến mại đã tồn tại"));
                die();
            }
            $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
            if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);

            if($promotionId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            else{
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $customerIds = $this->input->post('CustomerIds');
            if(!empty($customerIds)) $postData['IsAllCustomer'] = 0;
            else{
                $customerIds = array();
                $postData['IsAllCustomer'] = 1;
            }
            $this->load->model('Mpromotions');
            $promotionId = $this->Mpromotions->update($postData, $promotionId, $customerIds);
            echo json_encode(array('code' => 1, 'message' => "Cập nhật Mã khuyến mại thành công", 'data' => $promotionId));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function checkPromotionCode(){
        $this->checkUserLogin(true);
        $promotionCode = trim($this->input->post('PromotionCode'));
        $customerId = $this->input->post('CustomerId');
        if(!empty($promotionCode) && $customerId > 0){
            $this->load->model('Mpromotions');
            $this->loadModel(array('Mpromotions', 'Mcustomerpromotions', 'Morderpromotions'));
            $promotion = $this->Mpromotions->getBy(array('PromotionCode' => $promotionCode, 'StatusId' => STATUS_ACTIVED), true);
            if($promotion){
                $flag = $this->Mpromotions->checkPromotion($customerId, $promotion);
                if($flag){
                    $fields = array('BeginDate', 'EndDate', 'UseLimit', 'OrderLimit', 'StatusId', 'IsAllCustomer', 'CrUserId', 'CrDateTime', 'UpdateUserId', 'UpdateDateTime');
                    foreach($fields as $field) unset($promotion[$field]);
                    echo json_encode(array('code' => 1, 'message' => "Nhập Mã khuyến mại thành công", 'data' => $promotion));
                }
                else echo json_encode(array('code' => -1, 'message' => "Mã khuyến mại không còn hiệu lực"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Mã khuyến mại không tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
    
    public function getCountPromotionUse(){
        $this->checkUserLogin(true);
        $promotionIds = $this->input->post('PromotionIds');
        if(!empty($promotionIds)){
            $promotionIds = explode(',', $promotionIds);
            $this->load->model('Morderpromotions');
            $data = $this->Morderpromotions->getStatisticCount($promotionIds);
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}