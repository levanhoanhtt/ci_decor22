<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends MY_Controller {

	public function index(){
	   header("Content-Type: text/xml;");
	   $this->load->view("sitemap/list");
	}

	public function propertyCat(){
		header("Content-Type: text/xml;");
        $this->load->model('Mcategories');
	   	$this->load->view("sitemap/property_cat", array('listCategories' => $this->Mcategories->getListByItemType(3)));
	}

	public function property(){
		header("Content-Type: text/xml;");
        $this->load->model('Mproperties');
		$this->load->view("sitemap/property", array('listProperties' => $this->Mproperties->search(array('StatusId' => STATUS_ACTIVED))));
	}
}