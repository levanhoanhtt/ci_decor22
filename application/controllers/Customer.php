<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách khách hàng',
			array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'js/customer_list.js'))
            )
		);
		if($this->Mactions->checkAccess($data['listActions'], 'customer')) {
            $this->load->model('Mcustomers');
			$postData = $this->arrayFromPost(array('SearchText'));
			$rowCount = $this->Mcustomers->getCount($postData);
			$data['customerCount'] = $rowCount;
			$data['listCustomers'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listCustomers'] = $this->Mcustomers->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			} 
			$this->load->view('customer/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Thêm Khách hàng',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/customer_update.js'))
			)
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'customer/add')) {
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards'));
			$data['listProvinces'] = $this->Mprovinces->getList();
            $data['listCountries'] = $this->Mcountries->getList();
			$this->load->view('customer/add', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($customerId = 0){
        if ($customerId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Thông tin Khách hàng',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/customer_update.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
                $this->loadModel(array('Mcustomers', 'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards'));
                $customer = $this->Mcustomers->get($customerId);
                if ($customer) {
                    $data['customerId'] = $customerId;
                    $data['customer'] = $customer;
                    $data['listCountries'] = $this->Mcountries->getList();
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listDistricts'] = $this->Mdistricts->getList();
                }
                else {
                    $data['customerId'] = 0;
                    $data['txtError'] = "Không tìm thấy khách hàng";
                }
                $this->load->view('customer/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('customer');
    }

    public function delete(){
    	$user = $this->checkUserLogin();
    	$customerId = $this->input->post('CustomerId');
    	if($customerId > 0){
    		$this->load->model('Mcustomers');
    		$flag = $this->Mcustomers->changeStatus(0, $customerId, 'StatusId', $user['UserId']);
    		if($flag){
		        echo json_encode(array('code' => 1, 'message' => 'Xóa Khách hàng thành công'));
    		}else echo json_encode(array('code' => 0, 'message' => 'Có lỗi xảy ra, vui lòng thử lại.'));
    	}else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}