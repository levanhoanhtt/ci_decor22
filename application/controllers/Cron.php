<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {

	public function processImage($type = 1){
        $this->load->model('Morderphotos');
        $listOrderPhotos = $this->Morderphotos->getBy(array('StatusId' => 3));
        if(!empty($listOrderPhotos)) {
            require_once APPPATH . "/libraries/imageCrop.php";
            foreach ($listOrderPhotos as $op) {
                if (!empty($op['CropData'])) {
                    if(file_exists(IMAGE_PATH . $op['OriginalImage'])){
                        try {
                            $imageName = uniqid('decor22', true) . '.jpg';
                            $dir = IMAGE_PATH . $op['OrderId'] . '/';
                            @mkdir($dir, 0777, true);
                            //@system("/bin/chown -R nginx:nginx ".$dir);
                            if($type == 2) {
                                $imageUrl = $this->callProcessImage(IMAGE_PATH . $op['OriginalImage'], $op['CropData'], $op['Width'] > $op['Height'] ? $op['Height'] : $op['Width']);
                                if (!empty($imageUrl)) {
                                    $flag = file_put_contents($dir . $imageName, file($imageUrl));
                                    if ($flag !== false) {
                                        $this->Morderphotos->save(array('EditedImage' => $op['OrderId'] . '/' . $imageName, 'StatusId' => STATUS_ACTIVED), $op['OrderPhotoId']);
                                        echo getCurentDateTime() . ' => ' . $op['OrderPhotoId'] . ': process success' . PHP_EOL;
                                    }
                                    else echo getCurentDateTime() . ' => ' . $op['OrderPhotoId'] . ': process failed' . PHP_EOL;
                                    file_get_contents('http://localhost:3000/api/delete?url='.$imageUrl);
                                }
                                else echo getCurentDateTime() . ' => ' . $op['OrderPhotoId'] . ': process failed' . PHP_EOL;
                            }
                            else{
                                $imageCrop = new ImageCrop(IMAGE_PATH . $op['OriginalImage'], $op['CropData'], true);
                                $flag = $imageCrop->cropImage($dir . $imageName, $op['Width'] > $op['Height'] ? $op['Height'] : $op['Width'], $op['Width'], $op['Height']);
                                if ($flag) {
                                    $this->Morderphotos->save(array('EditedImage' => $op['OrderId'] . '/' . $imageName, 'StatusId' => STATUS_ACTIVED), $op['OrderPhotoId']);
                                    echo getCurentDateTime() . ' => ' . $op['OrderPhotoId'] . ': process success' . PHP_EOL;
                                }
                            }
                            usleep(5000);
                        }
                        catch (Exception $ex){
                            echo getCurentDateTime() . ' => ' . $op['OrderPhotoId'] . ': process failed: '.$ex->getMessage() . PHP_EOL;
                            continue;
                        }
                    }
                }
                else {
                    $this->Morderphotos->save(array('EditedImage' => $op['OriginalImage'], 'StatusId' => STATUS_ACTIVED), $op['OrderPhotoId']);
                    echo getCurentDateTime().' => '.$op['OrderPhotoId'] . ': empty CropData' . PHP_EOL;
                }
            }
        }
    }

    public function callProcessImage($imagePath, $cropData, $sizeOutput = 2048){
	    $sizeOutput = intval($sizeOutput);
	    if($sizeOutput > 2048) $sizeOutput = 2048;
        $ch = curl_init('http://localhost:3000/api/crop');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
            "pathInput" => "https://admin.decor22.vn/".$imagePath,
            "sizeOutput" => $sizeOutput,
            "cropData" => $cropData
        )));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);
        if(!empty($result)){
            $json = @json_decode($result, true);
            if($json && isset($json['success']) && $json['success'] == true) return isset($json['path']) ? $json['path'] : '';
        }
        return '';
    }

    /*public function deleteImage(){
        $this->load->model('Morderphotos');
        $listFiles = $this->getDirContents('/home/admin.decor22.vn/public_html/assets/uploads/images/');
        foreach ($listFiles as $f){
            $fileName = str_replace('/home/admin.decor22.vn/public_html/assets/uploads/images/', '', $f);
            $id = $this->Morderphotos->getIdByImage($fileName);
            if($id == 0){
                unlink($f);
                echo $f.PHP_EOL;
            }
        }
    }

    private  function getDirContents($dir, &$results = array()){
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            }
        }

        return $results;
    }*/

    public function autoSendMail(){
        $this->load->helper('common');
        $emailTo = 'datnguyen@decor22.vn';
        $emailCC = array('nguyenhuudat96@gmail.com');
        if(filter_var($emailTo, FILTER_VALIDATE_EMAIL)) {
            $this->loadModel(array('Morders', 'Morderphotos','Mcustomers', 'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards'));
            $orders = $this->Morders->getOrderInImages();
            $contentMail = '';
            $orderIds = array();
            for($i= 0; $i < count($orders); $i++){
                $customer = $this->Mcustomers->get($orders[$i]['CustomerId']);
                if($orders[$i]['QuantityImage'] > 0 && $customer){
                    $countryName = $this->Mcountries->getFieldValue(array('CountryId' => $customer['CountryId']), 'CountryName', '');
                    if(empty($countryName)) $countryName = 'Việt Nam';
                    $orderPhotos = $this->Morderphotos->getBy(array('StatusId' => STATUS_ACTIVED, 'OrderId' => $orders[$i]['OrderId']));
                    $orderCode = $this->Morders->genOrderCode($orders[$i]['OrderId']);
                    $transport = $orders[$i]['ShipCost'] > 0 ? priceFormat($orders[$i]['ShipCost']).'VNĐ': 'Miễn phí';
                    $mContent = '
                        <table style="width: 100%">
                            <tbody>
                                <tr>
                                    <td colspan="4">
                                    <img src="https://admin.decor22.vn/assets/icon/sussess.png"  style="color: #8BC34A;width: 60px; margin: 0 auto;display: block;">
                                    <h3 style=" margin: 12px 0 3px 0; font-size: 20px;text-transform: uppercase;text-align: center;">Đặt hàng thành công</h3>
                                    <p style=" margin: 12px 0 3px 0;font-size: 20px;text-transform: uppercase;text-align: center;">Cảm ơn bạn đã đặt hàng, đơn hàng của bạn đã được nhận.</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%!important">
                                    <p style="font-size: 14px;font-weight: 400;margin: 0;color: #8a8a8a;">Mã đơn hàng</p><span style="font-size: 15px;font-weight: 500;display: block;">'.$orderCode.'</span>
                                </td>
                                <td style="width: 50%!important"  colspan="3">
                                    <p style="font-size: 14px;font-weight: 400;margin: 0;color: #8a8a8a;">Ngày đơn hàng</p><span style="font-size: 15px;font-weight: 500;display: block;">'.ddMMyyyy($orders[$i]['OrderDate'], 'd/m/Y H:i').'</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%!important">
                                    <p style="font-size: 14px;font-weight: 400;margin: 0;color: #8a8a8a;">Tổng tiền</p>
                                <span style="font-size: 15px;font-weight: 500;display: block;">'.priceFormat($orders[$i]['PaidVN']).' VNĐ</span>
                                </td>
                                <td style="width: 50%!important" colspan="3">
                                    <p style="font-size: 14px;font-weight: 400;margin: 0;color: #8a8a8a;">Hình thức thanh toán</p>
                                <span style="font-size: 15px;font-weight: 500;display: block;">'.$transport.'</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: center;"><h2>Chi tiết đơn hàng</h2></td>
                            </tr>';
                        $count = 0;
                        for($y= 0; $y < count($orderPhotos); $y++){
                            $count = $count + 1;
                            $mContent .= '<tr>
                                    <td style="width: 20%!important"><span>'.$count.'</span></td>
                                    <td style="width: 30%!important">
                                        <img src="https://admin.decor22.vn/assets/uploads/images/'.$orderPhotos[$y]['EditedImage'].'" style="width: 46px; height: 46px; position: absolute;">
                                    </td>
                                    <td style="width: 20%!important"><span>x1</span></td>
                                    <td style="width: 30%!important"><span>'.priceFormat($orderPhotos[$y]['Price']).' VNĐ</span></td>
                                </tr>';
                        }

                        $mContent .= '<tr>
                                <td colspan="4" style="text-align: center;"><h2>Địa chỉ nhận hàng</h2></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <p style="font-weight:bold">'.$customer['FullName'].'</p>
                                    <p>SĐT: '.$customer['PhoneNumber'].' </p>
                                    <p>Email: '.$customer['Email'].' </p>
                                    <p><span>'.$customer['Address'].'</span> ' . $this->Mwards->getWardName($customer['WardId']).'<span>'.$this->Mdistricts->getFieldValue(array('DistrictId' => $customer['DistrictId']), 'DistrictName','').'</span>, <span>'.$this->Mprovinces->getFieldValue(array('ProvinceId' => $customer['ProvinceId']), 'ProvinceName','').'</span>, <span>'.$countryName.'</span></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    ';
                    if (filter_var($customer['Email'], FILTER_VALIDATE_EMAIL)){
                        $flagMail = $this->sendMail('xinchao@decor22.vn', 'ĐƠN HÀNG', $customer['Email'], 'decor22.vn', $mContent);
                        if ($flagMail) {
                            $orderIds[] = array(
                                'OrderId' => $orders[$i]['OrderId'],
                                'IsSendMail' => 1,
                            );
                            $contentMail .= $mContent;
                        }
                    }
                }
            }
            if(!empty($contentMail)){
                $flagMail = $this->sendMail('xinchao@decor22.vn', 'ĐƠN HÀNG', $emailTo, 'decor22.vn', $contentMail, array(), $emailCC);
                if ($flagMail) {
                    if(!empty($orderIds)) $this->db->update_batch('orders',$orderIds,'OrderId');
                    echo 'Send OK: ' . $emailTo . PHP_EOL;
                }
                else echo 'Send Failed: ' . $emailTo . PHP_EOL;
            }
            else echo 'NO Data'.PHP_EOL;
        }
        else echo 'Mail invalid'.PHP_EOL;
        // echo 'DONE'.PHP_EOL;
    }

    public function updatePhotoCount(){
        $this->load->model('Morders');
        $counts = $this->Morders->getPhotoCount();
        foreach ($counts as $id => $count){
            $this->Morders->save(array('PhotoCount' => $count), $id);
            echo $id.': '.$count.PHP_EOL;
        }
    }

    public function deleteOrder(){
        $this->loadModel(array('Morders', 'Morderphotos'));
        $listOrders = $this->Morders->search(array('CheckOrderStatusId' => 1, 'EndDate' => '2020-04-30'));
        foreach ($listOrders as $o){
            $this->Morders->save(array('OrderStatusId' => 0), $o['OrderId']);
            echo 'Delete order '.$o['OrderId'].PHP_EOL;
            if($o['PhotoCount'] > 0){
                $listOrderPhotos = $this->Morderphotos->getBy(array('OrderId' => $o['OrderId']));
                foreach($listOrderPhotos as $op){
                    if(!empty($op['OriginalImage'])){
                        unlink(IMAGE_PATH.$op['OriginalImage']);
                        echo 'Delete photo '.$op['OriginalImage'].PHP_EOL;
                    }
                    if(!empty($op['EditedImage'])){
                        unlink(IMAGE_PATH.$op['EditedImage']);
                        echo 'Delete photo '.$op['EditedImage'].PHP_EOL;
                    }
                }
            }
            echo '-------------'.PHP_EOL;
        }
    }
}