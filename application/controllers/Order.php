<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public function index($orderStatusId = 1){
        $title = 'Chờ xử lý';
        if($orderStatusId > 1) $title = 'Đã xử lý';
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách '.$title,
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/order_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'order/'.$orderStatusId)) {
            $this->loadModel(array('Morders', 'Mcustomers', 'Morderphotos'));
            $listCustomers = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCustomers'] = $listCustomers;
            $postData = $this->arrayFromPost(array('CustomerId', 'OrderStatusId', 'TotalPhoto', 'RefURL'));
            $postData['RefURL'] = str_replace(' ', '%20', $postData['RefURL']);
            $postData['CheckOrderStatusId'] = $orderStatusId;
            $rowCount = $this->Morders->getCount($postData);
            $data['listOrders'] = array();
            $data['CheckOrderStatusId'] = $orderStatusId;
            if($rowCount > 0){
                $pageCount = ceil($rowCount / DEFAULT_LIMIT);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $listOrders = $this->Morders->search($postData, DEFAULT_LIMIT, $page, $orderStatusId > 1 ? 'OrderDate' : 'OrderId');
        		for ($i = 0; $i < count($listOrders); $i++) {
        			$listOrders[$i]['OrderStatus'] = $listOrders[$i]['OrderStatusId'] > 0 ? $this->Mconstants->orderStatus[$listOrders[$i]['OrderStatusId']] : '';
        			$listOrders[$i]['CustomerName'] = $listOrders[$i]['CustomerId'] > 0 ? $this->Mcustomers->get($listOrders[$i]['CustomerId'], true, '', 'FullName')['FullName']: '';
        			$listOrders[$i]['OrderCode'] = $this->Morders->genOrderCode($listOrders[$i]['OrderId']);
        		}
                $data['listOrders'] = $listOrders;
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $data['permissionDelete'] = $this->Mactions->checkAccess($data['listActions'], 'order/delete');
            $this->load->view('order/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function view($orderId = 0){
        if($orderId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Chi tiết Đơn hàng',
                array(
                    'scriptFooter' => array('js' => array('vendor/plugins/lazyload/jquery.lazyload.min.js', 'js/order_update.js'))
                )
            );
            if($this->Mactions->checkAccess($data['listActions'], 'order/view')){
                $this->loadModel(array('Morders', 'Mcustomers', 'Morderphotos', 'Mprovinces', 'Mdistricts', 'Mwards', 'Mpromotions', 'Mconfigs'));
                $order = $this->Morders->get($orderId);
                if ($order && $order['OrderStatusId'] > 0){
                    $data['permissionActiveStatus'] = $this->Mactions->checkAccess($data['listActions'], 'order/activeStatus');
                	$order['OrderCode'] = $this->Morders->genOrderCode($orderId);
                    $data['orderId'] = $orderId;
                    $data['order'] = $order;
                    $data['listOrderPhotos'] = $this->Morderphotos->getBy(array('OrderId' => $orderId, 'StatusId >' => 0));
                    $data['totalOrderOfCustomer'] = $order['CustomerId'] > 0 ? $this->Morders->getCount(array('CustomerId' => $order['CustomerId'])) : 0;
                    $data['customer'] = $this->Mcustomers->get($order['CustomerId']);
                    $urlParam = array();
                    if(!empty($order['RefURL'])) parse_str(parse_url($order['RefURL'], PHP_URL_QUERY), $urlParam);
                    $data['urlParam'] = $urlParam;
                }
                else {
                    $data['canEdit'] = false;
                    $data['orderId'] = 0;
                    $data['txtError'] = "Không tìm thấy Đơn hàng";
                }
                $this->load->view('order/view', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('order');
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $data = $this->commonData($user, '', array());
        $permissionDelete = $this->Mactions->checkAccess($data['listActions'], 'order/delete');
        $permissionActiveStatus = $this->Mactions->checkAccess($data['listActions'], 'order/activeStatus');
        if($permissionDelete || $permissionActiveStatus){
            $orderId = $this->input->post('OrderId');
            $orderStatusId = $this->input->post('OrderStatusId');
             if($orderId){
                $this->load->model('Morders');
                $flag = $this->Morders->changeStatus($orderStatusId, $orderId, 'OrderStatusId', $user['UserId']);
                if($flag) echo json_encode(array('code' => 1, 'message' => 'Cập nhật trạng thái thành công.'));
                else echo json_encode(array('code' => 0, 'message' => 'Có lỗi xảy ra, vui lòng thử lại.'));
            }
            else echo json_encode(array('code' => -1, 'message' => 'Có lỗi xảy ra, vui lòng thử lại.'));
        }
        else echo json_encode(array('code' => -1, 'message' => 'Có lỗi xảy ra, vui lòng thử lại.'));
    }

    public function getCountPhoto(){
        $user = $this->checkUserLogin(true);
        $orderIds = $this->input->post('OrderIds');
        if(!empty($orderIds)){
            $this->load->model('Morderphotos');
            $data = $this->Morderphotos->getCountPhoto(explode(',', $orderIds));
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $orderId = $this->input->post('OrderId');
        $comment = $this->input->post('Comment');
        if($orderId){
            $this->load->model('Morders');
            $postData = array(
                'Comment' => $comment,
                'UpdateUserId' => $user['UserId'],
                'UpdateDateTime' => getCurentDateTime(),
            );
            $flag = $this->Morders->save($postData, $orderId);
            if($flag) echo json_encode(array('code' => 1, 'message' => 'Thêm ghi chú thành công.'));
            else echo json_encode(array('code' => 0, 'message' => 'Có lỗi xảy ra, vui lòng thử lại.'));
        }
        else echo json_encode(array('code' => -1, 'message' => 'Có lỗi xảy ra, vui lòng thử lại.'));
    }

    function zipFile($orderId = 0) {
        if($orderId){
            $this->load->model(array('Morders', 'Morderphotos', 'Mcustomers', 'Mwards', 'Mprovinces', 'Mdistricts', 'Mcountries'));
            $order = $this->Morders->get($orderId);
            if($order['OrderStatusId'] > 1){
                # create new zip opbject
                $zip = new ZipArchive;
                # create a temp file & open it
                $tmpFile = tempnam('assets/tmp/','');
                $zip->open($tmpFile, ZipArchive::CREATE);
                /*$info = 'Chưa có thông tin';
                if($order['CustomerId']){
                    $listDistricts = $this->Mdistricts->getList();
                    $listProvinces = $this->Mprovinces->getList();
                    $listCountries = $this->Mcountries->getList();
                    $customer = $this->Mcustomers->get($order['CustomerId']);
                    $countryName = $this->Mconstants->getObjectValue($listCountries, 'CountryId', $customer['CountryId'], 'CountryName');
                    if(!$countryName) $countryName = 'Việt Nam';
                    $info =  '- Họ và  tên: '.$customer['FullName']. PHP_EOL;
                    $info .= '- Số điện thoại: '.$customer['PhoneNumber']. PHP_EOL;
                    $info .= '- Địa chỉ: '. $this->Mwards->getWardName($customer['WardId']). PHP_EOL;
                    $info .= '- Quận/Huyện: '.$this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $customer['DistrictId'], 'DistrictName'). PHP_EOL;
                    $info .= '- Tỉnh/Thành: '.$this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $customer['ProvinceId'], 'ProvinceName'). PHP_EOL;
                    $info .= '- Quốc gia: '. $countryName;
                }*/
                // $zip->addFromString('thong_tin_khach_hang.txt', $info);

                // get all data image in order photo
                $photoStyleId = 0;
                $orderPhotos = $this->Morderphotos->getBy(array('OrderId' => $order['OrderId'], 'StatusId' => STATUS_ACTIVED));
                foreach($orderPhotos as $photo){
                    $photoStyleId = $photo['PhotoStyleId'];
                    # get image data;
                    $editedImage = 'assets/uploads/images/'.$photo['EditedImage'];
                     # download file
                    $downloadFile = file_get_contents($editedImage);
                    $zip->addFromString(basename($editedImage), $downloadFile);
                }
                $zip->close();
                if(isset($this->Mconstants->photoStyles[$photoStyleId])) $zipName = $this->Morders->genOrderCode($orderId).'-'.makeSlug($this->Mconstants->photoStyles[$photoStyleId]).'.zip';
                else $zipName = $this->Morders->genOrderCode($orderId).'.zip';
                # send the file to the browser as a download
                header('Content-disposition: attachment; filename='.$zipName);
                header('Content-type: application/zip');
                readfile($tmpFile);
                unlink($tmpFile);
            }
        }
        
    }

     public function delete(){
        $this->checkUserLogin();
        $orderId = $this->input->post('OrderId');
        if($orderId > 0){
            $this->load->model('Morders');
            $flag = $this->Morders->changeStatus(0, $orderId, 'OrderStatusId');
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

}