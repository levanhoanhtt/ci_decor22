<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Slider trang chủ',
			array('scriptFooter' => array('js' => array('js/slider.js')))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'slider')) {
			$this->load->model('Msliders');
			$data['listSliders'] = $this->Msliders->getList(1);
			$this->load->view('setting/slider', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('SliderName', 'SliderDesc', 'SliderImage', 'SliderIcon', 'DisplayOrder', 'SliderTypeId', 'SliderNickname'));
		$sliderImage = $postData['SliderImage'];
		if(!empty($sliderImage) && !empty($postData['SliderIcon'])) {
			$postData['SliderImage'] = replaceFileUrl($sliderImage);
			$postData['SliderIcon'] = replaceFileUrl($postData['SliderIcon']);
			$sliderId = $this->input->post('SliderId');
			$this->load->model('Msliders');
			$flag = $this->Msliders->update($postData, $sliderId);
			if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật slider thành công"));
			else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

	public function changeDisplayOrder(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('DisplayOrder', 'SliderTypeId'));
		$sliderId = $this->input->post('SliderId');
		if($sliderId > 0 && $postData['SliderTypeId'] > 0 && $postData['DisplayOrder'] > 0){
			$this->load->model('Msliders');
			$flag = $this->Msliders->update($postData, $sliderId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Thay đổi thứ tự thành công"));
			else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

	public function delete(){
		$this->checkUserLogin(true);
		$sliderId = $this->input->post('SliderId');
		if($sliderId > 0){
			$this->load->model('Msliders');
			$flag = $this->Msliders->delete($sliderId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa ảnh thành công"));
			else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

}
