 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frequentquestion extends MY_Controller {

	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách câu hỏi',
            array('scriptFooter' => array('js' => 'js/frequent_question.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'frequentquestion')) {
            $this->load->model('Mfrequentquestions');
            $data['listFrequentQuestions'] = $this->Mfrequentquestions->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/frequent_question', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('QuestionName', 'Answer'));
        if(!empty($postData['QuestionName']) && !empty($postData['Answer'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $frequentQuestionId = $this->input->post('FrequentQuestionId');
            $this->load->model('Mfrequentquestions');
            $flag = $this->Mfrequentquestions->save($postData, $frequentQuestionId);
            if ($flag > 0) {
                $postData['FrequentQuestionId'] = $flag;
                $postData['IsAdd'] = ($frequentQuestionId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật câu hỏi thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $frequentQuestionId = $this->input->post('FrequentQuestionId');
        if($frequentQuestionId > 0){
            $this->load->model('Mfrequentquestions');
            $flag = $this->Mfrequentquestions->changeStatus(0, $frequentQuestionId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa câu hỏi thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}