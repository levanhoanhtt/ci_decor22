<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Accesslog extends MY_Controller {

	public function index(){
        $user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Access Log'
		);
		if($this->Mactions->checkAccess($data['listActions'], 'accesslog')) {
		    $listLogs = array();
            $fileName = FCPATH.'assets/logs/access.log';
            if(file_exists($fileName)){
                $logs = file_get_contents($fileName);
                $logs = preg_split('/\n/', $logs);
                foreach($logs as $l){
                    if(strpos($l, 'utm_source') != false){
                        $parts = explode('- -', $l);
                        if(count($parts) == 2){
                            $parts1 = explode('+0700]', $parts[1]);
                            if(count($parts1) == 2) {
                                $parts2 = explode('"', $parts1[1]);
                                $parts3 = array();
                                foreach($parts2 as $str){
                                    if(!empty(trim($str))) $parts3[] = $str;
                                }
                                if(count($parts3) == 4){
                                    if(strpos($parts3[0], 'utm_source') != false){
                                        $url = trim(str_replace(array('GET', 'HTTP/2.0'), '', $parts3[0]));
                                        parse_str(parse_url($url, PHP_URL_QUERY), $parts4);
                                        $listLogs[] = array(
                                            'ip' => trim($parts[0]),
                                            'time' => trim(str_replace('[', '', $parts1[0])),
                                            'url' => $url,
                                            'agent' => $parts3[3],
                                            'param' => $parts4
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $data['listLogs'] = array_reverse($listLogs);
			$this->load->view('setting/access_log', $data);
		}
		else $this->load->view('user/permission', $data);
	}
}
