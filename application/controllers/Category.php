<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

    public function index($itemTypeId = 0){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Chuyên mục',
            array('scriptFooter' => array('js' => array('js/category.js')))
        );
        if(!in_array($itemTypeId, array(3, 4))) $itemTypeId = 1;
        if($this->Mactions->checkAccess($data['listActions'], 'category/'.$itemTypeId)) {
            $itemTypeName = $this->getItemTypeName($itemTypeId);
            $data['itemTypeId'] = $itemTypeId;
            $data['itemTypeName'] = $itemTypeName;
            $data['title'] = 'Danh sách '.$itemTypeName;
            $this->load->model('Mcategories');
            $data['listCategories'] = $this->Mcategories->getListByItemType($itemTypeId, false, true);
            $this->load->view('category/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add($itemTypeId = 0){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Chuyên mục',
            array('scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'js/category_update.js')))
        );
        if(!in_array($itemTypeId, array(3, 4))) $itemTypeId = 1;
        if($this->Mactions->checkAccess($data['listActions'], 'category/'.$itemTypeId)) {
            $itemTypeName = $this->getItemTypeName($itemTypeId);
            $data['itemTypeId'] = $itemTypeId;
            $data['itemTypeName'] = $itemTypeName;
            $data['title'] = 'Thêm '.$itemTypeName;
            $this->load->model('Mcategories');
            $data['listCategories'] = $this->Mcategories->getBy(array('ItemTypeId' => $itemTypeId, 'StatusId' => STATUS_ACTIVED, 'ParentCategoryId' => 0));
            $this->load->view('category/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($categoryId){
        if($categoryId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Sửa Chuyên mục',
                array('scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'js/category_update.js')))
            );
            $this->load->model('Mcategories');
            $category = $this->Mcategories->get($categoryId);
            if($category && $category['StatusId'] > 0) {
                if ($this->Mactions->checkAccess($data['listActions'], 'category/'.$category['ItemTypeId'])) {
                    $data['categoryId'] = $categoryId;
                    $data['category'] = $category;
                    $itemTypeId = $category['ItemTypeId'];
                    $itemTypeName = $this->getItemTypeName($itemTypeId);
                    $data['itemTypeId'] = $itemTypeId;
                    $data['itemTypeName'] = $itemTypeName;
                    $data['title'] = 'Sửa ' . $itemTypeName;
                    $data['listCategories'] = $this->Mcategories->getBy(array('ItemTypeId' => $itemTypeId, 'StatusId' => STATUS_ACTIVED, 'ParentCategoryId' => 0));
                    $this->load->view('category/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['categoryId'] = 0;
                $data['txtError'] = "Không tìm thấy chuyên mục";
            }
        }
        else redirect('category');
    }

    private function getItemTypeName($itemTypeId){
        $itemTypeName = '';
        if($itemTypeId == 3) $itemTypeName = 'Property Type';
        //elseif($itemTypeId == 2) $itemTypeName = 'Agent';
        elseif($itemTypeId == 4) $itemTypeName = 'chuyên mục bài viết';
        return $itemTypeName;
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('CategoryName', 'CategoryDesc', 'CategorySlug', 'ItemTypeId', 'CategoryTypeId', 'CategoryImage', 'StatusId' , 'ParentCategoryId', 'DisplayOrder'));
        if(!empty($postData['CategoryName']) && $postData['ItemTypeId'] > 0 && $postData['CategoryTypeId'] > 0 && $postData['StatusId'] > 0 && $postData['DisplayOrder'] > 0) {
            if (empty($postData['CategorySlug'])) $postData['CategorySlug'] = makeSlug($postData['CategoryName']);
            else $postData['CategorySlug'] = makeSlug($postData['CategorySlug']);
            $postData['CategoryImage'] = replaceFileUrl($postData['CategoryImage']);
            $categoryId = $this->input->post('CategoryId');
            if ($categoryId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
                if($categoryId == $postData['ParentCategoryId']){
                    echo json_encode(array('code' => -1, 'message' => "Chuyên mục cha không hợp lệ"));
                    die();
                }
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $this->load->model('Mcategories');
            $categoryId = $this->Mcategories->update($postData, $categoryId);
            if ($categoryId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Chuyên mục thành công", 'data' => $categoryId));
            else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
        }
        else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
    }

    public function changeDisplayOrder(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('DisplayOrder', 'ItemTypeId', 'CategoryTypeId', 'ParentCategoryId'));
        $categoryId = $this->input->post('CategoryId');
        if($categoryId > 0 && $postData['DisplayOrder'] > 0 && $postData['ItemTypeId'] > 0 && $postData['CategoryTypeId'] > 0){
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDateTime'] = getCurentDateTime();
            $this->load->model('Mcategories');
            $flag = $this->Mcategories->update($postData, $categoryId);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Thay đổi thứ tự thành công"));
            else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
        }
        else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $categoryId = $this->input->post('CategoryId');
        $statusId = $this->input->post('StatusId');
        if($categoryId > 0 && $statusId >= 0){
            $this->load->model('Mcategories');
            $flag = $this->Mcategories->changeStatus($statusId, $categoryId, '', $user['UserId']);
            if($flag) {
                $msg = 'Xóa chuyên mục thành công';
                $statusName = '';
                if ($statusId > 0) {
                    $msg = 'Thay đổi trạng thái thành công';
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $msg, 'data' => $statusName));
            }
            else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
        }
        else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
    }
}