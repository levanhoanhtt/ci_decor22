<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

	public function index(){
		header('Content-Type: application/json');
		$categoryId = $this->input->get('categoryId');
		$page = $this->input->get('pageId');
		$this->loadModel(array('Marticles'));
		$postData = array('CategoryId' => $categoryId);
		$rowCount = $this->Marticles->getCount($postData);
		$listArticles = array();
		$perPage = DEFAULT_LIMIT;
		if($rowCount > 0){
            $pageCount = ceil($rowCount / $perPage);
            if(!is_numeric($page) || $page < 1) $page = 1;
            $listArticles = $this->Marticles->search($postData, $perPage, $page);
            for ($i=0; $i < count($listArticles) ; $i++) { 
            	$listArticles[$i]['ArticleSlug'] = $this->Mconstants->getUrl($listArticles[$i]['ArticleSlug'], $listArticles[$i]['ArticleId'], 4);
            	$image = IMAGE_PATH.(empty($listArticles[$i]['ArticleImage']) ? NO_IMAGE : $listArticles[$i]['ArticleImage']);
            	$listArticles[$i]['ArticleImage'] = base_url().$image;
            	$listArticles[$i]['CrUserName'] = $this->Musers->getFieldValue(array('UserId' => $listArticles[$i]['CrUserId']), 'FullName');
            	unset($listArticles[$i]['CrUserId'], $listArticles[$i]['UpdateUserId'], $listArticles[$i]['UpdateDateTime']);
            }
        }
		echo json_encode(array('page' => $page, 'limit' => $perPage, 'datas' => $listArticles));
	}
}