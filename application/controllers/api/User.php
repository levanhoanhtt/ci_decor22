<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function updateProfile(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('UserPass', 'NewPass', 'FullName', 'Email', 'GenderId', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'BirthDay', 'PhoneNumber', 'Avatar', 'Facebook'));
		if(!empty($postData['FullName']) && $postData['GenderId'] > 0) {
			$flag = false;
			if (!empty($postData['NewPass'])) {
				if ($user['UserPass'] == md5($postData['UserPass'])) {
					$flag = true;
					$postData['UserPass'] = md5($postData['NewPass']);
					unset($postData['NewPass']);
				}
				else echo json_encode(array('code' => -1, 'message' => "Old Password is wrong"));
			}
			else {
				$flag = true;
				unset($postData['UserPass']);
				unset($postData['NewPass']);
			}
			if ($flag) {
				if($this->Musers->checkExist($user['UserId'], $postData['Email'], $postData['PhoneNumber'])) {
					echo json_encode(array('code' => -1, 'message' => "Username or Phone number is existed"));
				}
				else {
					$postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
					if(empty($postData['Avatar'])) $postData['Avatar'] = NO_IMAGE;
					else $postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
					$postData['UpdateUserId'] = $user['UserId'];
					$postData['UpdateDateTime'] = getCurentDateTime();
					$userId = $this->Musers->update($postData, $user['UserId'], false);
					if($userId > 0){
						$user = array_merge($user, $postData);
						$this->session->set_userdata('user', $user);
						echo json_encode(array('code' => 1, 'message' => "Update profile success"));
					}
					else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
				}
			}
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$userId = $this->input->post('UserId');
		$statusId = $this->input->post('StatusId');
		if($userId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
			$flag = $this->Musers->changeStatus($statusId, $userId, '', $user['UserId']);
			if($flag) {
				$statusName = "";
				if($statusId == 0) $txtSuccess = "Delete {$this->input->post('UserTypeName')} success";
				else{
					$txtSuccess = "Change status success";
					$statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
				}
				echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
			}
			else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

	public function saveUser(){
		$user = $this->session->userdata('user');
		$postData = $this->arrayFromPost(array('UserPass', 'UserName', 'FullName', 'Email', 'GenderId', 'StatusId', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'BirthDay', 'PhoneNumber', 'Facebook', 'Avatar'));

		if(!empty($postData['FullName']) && $postData['GenderId'] > 0 && $postData['StatusId'] > 0 ) {
            $userId = $this->input->post('UserId');
			if ($this->Musers->checkExist($userId, $postData['Email'], $postData['PhoneNumber'])) {
				echo json_encode(array('code' => -1, 'message' => "Username or Phone number is existed"));
			}
			else {
				$postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
				if(empty($postData['Avatar'])) $postData['Avatar'] = NO_IMAGE;
				else $postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
				$userPass = $postData['UserPass'];
				if ($userId == 0){
					$postData['UserPass'] = md5($userPass);
					$postData['CrUserId'] = ($user) ? $user['UserId'] : 0;
					$postData['CrDateTime'] = getCurentDateTime();
				}
				else {
					unset($postData['UserPass']);
					$newPass = trim($this->input->post('NewPass'));
					if (!empty($newPass)) $postData['UserPass'] = md5($newPass);
					$postData['UpdateUserId'] = ($user) ? $user['UserId'] : 0;
					$postData['UpdateDateTime'] = getCurentDateTime();
				}
                $groupIds = $this->input->post('GroupIds');
				if(!is_array($groupIds)) $groupIds = array();
				$userId = $this->Musers->update($postData, $userId, true, $groupIds);
				if ($userId > 0) {
					if($user && $user['UserId'] == $userId){
						$user = array_merge($user, $postData);
						$this->session->set_userdata('user', $user);
					}
					if ($this->input->post('IsSendPass') == 'on') {
						$message = "Xin chào {$postData['FullName']}<br/>Your Login info: <br/>URL: " . base_url() . "<br/>Username: {$postData['PhoneNumber']}<br/>Password: {$userPass}";
						$configs = $this->session->userdata('configs');
						if (!$configs) $configs = array();
						$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'ricky@gmail.com';
						$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Ricky';
						$this->sendMail($emailFrom, $companyName, $postData['Email'], 'Login info', $message);
					}
					echo json_encode(array('code' => 1, 'message' => "Update staff success", 'data' => $userId));
				}
				else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

	public function checkLogin(){
        header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		//log_message('error', json_encode($_POST));
		//log_message('error', json_encode(file_get_contents('php://input')));
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'IsRemember', 'IsGetConfigs'));
		$userName = $postData['UserName'];
		$userPass = $postData['UserPass'];
		if(!empty($userName) && !empty($userPass)) {
			$configs = array();
			$user = $this->Musers->login($userName, $userPass);
			if ($user) {
				if(empty($user['Avatar'])) $user['Avatar'] = NO_IMAGE;
				$this->session->set_userdata('user', $user);
				if ($postData['IsGetConfigs'] == 1) {
					$this->load->model('Mconfigs');
					$configs = $this->Mconfigs->getListMap();
					$this->session->set_userdata('configs', $configs);
				}
				if ($postData['IsRemember'] == 'on') {
					$this->load->helper('cookie');
					$this->input->set_cookie(array('name' => 'userName', 'value' => $userName, 'expire' => '86400'));
					$this->input->set_cookie(array('name' => 'userPass', 'value' => $userPass, 'expire' => '86400'));
				}
                $user['SessionId'] = uniqid();
				echo json_encode(array('code' => 1, 'message' => "Login success", 'data' => array('User' => $user, 'Configs' => $configs, 'message' => "Login success")));
			}
			else echo json_encode(array('code' => 0, 'message' => "Login failed"));
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

	public function forgotPass(){
		header('Content-Type: application/json');
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$userPass = bin2hex(mcrypt_create_iv(5, MCRYPT_DEV_RANDOM));
				$message = "Hi {$user['FullName']}.<br/> Your new password is {$userPass}";
				$this->load->model('Mconfigs');
				$configs = $this->Mconfigs->getListMap();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'ricky@gmail.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Ricky';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Your new password', $message);
				if($flag){
					$this->Musers->save(array('UserPass' => md5($userPass)), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Sent password to {$email}", 'data' => array('message' => "Sent password to {$email}")));
				}
				else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
			}
			else echo json_encode(array('code' => 0, 'message' => "User is not existed"));
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}

	public function logout(){
		$fields = array('user', 'configs');
		foreach($fields as $field) $this->session->unset_userdata($field);
	}

	public function requestSendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				$message = "Hello {$user['FullName']}<br/>Click ".base_url('user/changePass/'.$token).' to change password.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'ricky@gmail.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Ricky';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Get new password', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Check your mail and follow the guide"));
				}
				else echo json_encode(array('code' => 0, 'message' => ERROR_COMMON_MESSAGE));
			}
			else echo json_encode(array('code' => 0, 'message' => "User is not existed"));
		}
		else echo json_encode(array('code' => -1, 'message' => ERROR_COMMON_MESSAGE));
	}
}