<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	public function save(){
		header('Content-Type: application/json');
		$postData = $this->arrayFromPost(array('FirstName', 'LastName', 'Email')); //, 'PhoneNumber'
		$message = $this->getMessageError($postData);
        if (!empty($message)) {
            echo json_encode(['code' => 0, 'message' => $message['message'], 'field' => $message['field']]);
            die;
        }
        $this->loadModel(array('Mcustomers'));
        if($this->Mcustomers->checkExist(0, $postData) > 0) {
			echo json_encode(array('code' => -1, 'message' => "Email đã tồn tại trong hệ thống"));
		}
		$crDateTime = getCurentDateTime();
		$postData['CrDateTime'] = $crDateTime;
		$postData['StatusId'] = STATUS_ACTIVED;
		$customerId = $this->Mcustomers->save($postData);
		if($customerId > 0){
			echo json_encode(array('code' => 1, 'message' => "Thêm Khách hàng thành công", 'data' => $customerId));
		} 
		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));

	}

	function getMessageError($postData){
        if (isset($postData['FirstName']) && empty($postData['FirstName'])) {
            return [
                'message' => 'Tên không được phép để trống',
                'field' => 'LastName'
            ];
        }
        if (isset($postData['LastName']) && empty($postData['LastName'])) {
            return [
                'message' => 'Họ không được phép để trống',
                'field' => 'LastName'
            ];
        }
        // if (isset($postData['PhoneNumber']) && empty($postData['PhoneNumber'])) {
        //     return [
        //         'message' => 'Số điện thoại không được phép để trống',
        //         'field' => 'PhoneNumber'
        //     ];
        // }
        $email = strtolower($postData['Email']);
        if (isset($postData['Email']) && empty($email)) {
            return [
                'message' => 'Email không được phép để trống',
                'field' => 'Email'
            ];
        }

        $checkEmail = isValidEmail($email);
        if (!$checkEmail) {
            return [
                'message' => 'Định dạng email chưa đúng',
                'field' => 'Email'
            ];
        }

        // $checkPhone = checkPhoneNumber($postData['PhoneNumber']);
        // if (!$checkPhone) {
        //     return [
        //         'message' => 'Định dạng số điện thoại chưa đúng',
        //         'field' => 'PhoneNumber'
        //     ];
        // }
        return '';
    }

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('FullName', 'PhoneNumber', 'Email', 'GenderId', 'BirthDay', 'CountryId', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'Password'));
		if(!empty($postData['FullName'])) {
			$customerId = $this->input->post('CustomerId');
			$this->loadModel(array('Mcustomers'));

			if($this->Mcustomers->checkExist($customerId, $postData) > 0) {
				echo json_encode(array('code' => -1, 'message' => "Email hoặc Số điện thoại đã tồn tại trong hệ thống"));
			}
			else{
				if (!empty($postData['BirthDay'])) $postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
				$crDateTime = getCurentDateTime();
				
				if($customerId > 0){
					$postData['UpdateUserId'] = $user['UserId'];
					$postData['UpdateDateTime'] = $crDateTime;
					if(!empty($postData['Password'])) $postData['Password'] = md5($postData['Password']);
					else unset($postData['Password']);
				}
				else {
					$postData['StatusId'] = STATUS_ACTIVED;
					$postData['CrUserId'] = $user['UserId'];
					$postData['CrDateTime'] = $crDateTime;
					if(!empty($postData['Password'])) $postData['Password'] = md5($postData['Password']);
					else $postData['Password'] = md5('123456789');
				}
                $customerId = $this->Mcustomers->save($postData, $customerId, array('BirthDay', 'UpdateUserId', 'UpdateDateTime'));
				if($customerId > 0){
					echo json_encode(array('code' => 1, 'message' => "Cập nhật Khách hàng thành công", 'data' => $customerId));
				} 
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}