<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MY_Controller {

	public function __construct(){
		parent::__construct();
        $this->loadModel(array('Mconfigs'));
	}

    private function openAllCors(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        //header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        header('Access-Control-Allow-Headers: *');
    }

	public function init($langCode = 'vn'){
		try { 
			$this->openAllCors();
			$isLangVn = !($langCode == 'en');
            $this->loadModel(array('Mconfigs', 'Msliders', 'Mcategories'));//, 'Mfrequentquestions', 'Mmenuitems'));
			$config =  $this->Mconfigs->getListMap();
			$listSliders = $this->Msliders->getApiList(1);
			// $listCategories = $this->Mcategories->getListByItemType(4, false, true);
			//$listQuestions = $this->Mfrequentquestions->getBy(array('StatusId' => STATUS_ACTIVED));
			$arrQuestion = array();
			/*for ($z=0; $z < count($listQuestions); $z++) {
				$arrQuestion[] = array(
					'question' => $listQuestions[$z]['QuestionName'],
					'answer' => $listQuestions[$z]['Answer'],
				);
			}*/
			$arrMenuBottom = array();
			$arrMenuHeader = array();
			/*$listMenuItems =  $this->Mmenuitems->getListByPosition(array(1, 2));
			for ($i=0; $i < count($listMenuItems[1]); $i++) { 
				$arrMenuBottom[] = array(
					//'id' => $listCategories[$i]['CategoryId'],
					'name' => $listMenuItems[1][$i]['ItemName'],
					'link' => $listMenuItems[1][$i]['ItemUrl'], //$this->Mconstants->getUrl($listCategories[$i]['CategorySlug'], $listCategories[$i]['CategoryId'], 1),
					'isRouter' => true,
                    'showMobile' => true
				);
			}
			for ($z=0; $z < count($listMenuItems[2]); $z++) { 
				$arrMenuHeader[] = array(
						'name' => $listMenuItems[2][$z]['ItemName'],
						'link' => $listMenuItems[2][$z]['ItemUrl'], 
					);
			}*/
			$listConfigs = array(
				'settings' => array(
					'backgroundColor' => $config['BACKGROUND_COLOR'],
					'mainColor' => $config['MAIN_COLOR'],
				),
				'home' => array(
					'logoUrl' => base_url(IMAGE_PATH.$config['LOGO_IMAGE']),
					'description' => $isLangVn ? $config['META_DESC'] : 'Stick to the wall photo frames, upload photos from your phone',
					'videoUrl' => $config['YOUTUBE_LINK_HEADER'],
					'descriptionVideo' => $config['YOUTUBE_TITLE_FOOTER'],
					// 'videoTitle' => $config['YOUTUBE_TITLE_HEADER'],
					'videoIconUrl' => base_url(IMAGE_PATH.$config['YOUTUBE_IMAGE_FOOTER']),
					'sectionPrice' => array(
						'title' => $config['PRICE_TITLE'],
						'description' => $config['PRICE_DESCRIPTION'],
					),
					'sectionReviews' => array(
						'title' => $config['REVIEW_TITLE'],
						'desc' => $config['REVIEW_DESCRIPTION'],
						'reviews' => $listSliders,
					),
					'sectionPrint' => array(
						'title' => $config['INTRODUCE_TITLE_1'],
						'description' => $config['INTRODUCE_INFO_1'],
					),
					'sectionRate' => array(
						'imageUrl' => base_url(IMAGE_PATH.$config['IMAGE_RATE_START']),
						'info' => $config['EVALUATE_INFO'],
						'description' => $config['INTRODUCE_INFO_2'],
					),
					'sectionMenuBottom' => array(
						'menu' => $arrMenuBottom,
						'copyright' => $config['COPYRIGHT'],
					),
					'languages' => array(
						array(
							'iconUrl' => base_url('assets/icon/vietnam.png'),
							'name' => 'Việt Nam',
							'code' => 'vn',
						),
                        array(
                            'iconUrl' => base_url('assets/icon/en.svg'),
                            'name' => 'English',
                            'code' => 'en',
                        ),
					),
					'startButton' => $isLangVn ? "Tải ảnh lên" : "Upload your photos",
					'startButtonExt' => $isLangVn ? "Tải ảnh lên" : "Upload your photos",//"Chọn hình theo phong cách của bạn",

				),
				'register' => array(
					'title' => "Nhanh tay đăng ký",
					'backgroundImageUrl' => base_url('assets/icon/Mixtiles_Scene_9.jpg'),
					'controls' => array(
						'inputFirstName' => array('placeholder' => 'Tên'),
						'inputLastName' => array('placeholder' => 'Họ'),
						'inputEmail' => array('placeholder' => 'Email'),
					),
					'btnSubmit' => array('text' => 'Chọn ảnh'),
				),
				'header' => array(
					'backIconUrl' => base_url('assets/icon/backButton.svg'),
					'menuIconUrl' => base_url('assets/icon/menuIcon.svg'),
					'webMenu' => array(
						'logo' => 	base_url('assets/icon/logo.png'),
						'menus' => $arrMenuHeader,
					),
				),
				'design' => array(
					'styles' => array(
						 array(
						 	'frameUrl'	=>	base_url('assets/icon/white.svg'),
						 	'iconUrl'	=>	base_url('assets/icon/cleanIcon.svg'),
						 	'name'	=>	$isLangVn ? 'Trắng' : 'Clean',
						 	'code'	=>	'clean',
						 	'isPadding'	=>	false,
						 ),
						 array(
						 	'frameUrl'	=>	base_url('assets/icon/white.svg'),
                             'iconUrl'	=>	base_url('assets/icon/everIcon.svg'),
						 	'name'	=>	$isLangVn ? 'Trắng viền' : 'Ever',
						 	'code'	=>	'ever',
						 	'isPadding'	=>	true,
						 ),
						array(
							'frameUrl'	=>	base_url('assets/icon/black.svg'),
							'iconUrl'	=>	base_url('assets/icon/boldIcon.svg'),
							'name'	=>	$isLangVn ? 'Đen' : 'Bold',
							'code'	=>	'bold',
							'isPadding'	=>	false,
						),
						array(
						 	'frameUrl'	=>	base_url('assets/icon/black.svg'),
						 	'iconUrl'	=>	base_url('assets/icon/classicIcon.svg'),
						 	'name'	=>	$isLangVn ? 'Đen viền' : 'Classic',
						 	'code'	=>	'classic',
						 	'isPadding'	=>	true,
						),
					),
					'btnSubmit' => array('text' => $isLangVn ? 'Đặt hàng' : 'Checkout'),
					'backgroundImageUrl' => base_url('assets/icon/noTilesImage@2x.jpg'),
					'upload' => array(
						'iconUrl' => base_url('assets/icon/uploadIcon.svg'),
						'text' => $isLangVn ? "Tải ảnh lên" : "Upload your photos",
					),
					'title' => $isLangVn ? 'Chỉnh sửa hình ảnh' : 'Adjust',
					'zoomPopup' => array(
						'done'	=>	$isLangVn ? 'Xong' : 'Done',
						'title'	=> $isLangVn ? 'Điều chỉnh hình ảnh' : 'Adjust',
						'pinchAndZoom'	=>	$isLangVn ? 'Chụm và thu phóng' : 'PINCH AND ZOOM',

					),
					'editMenuPopup' => array(
						'show' => false,
						'menus' => array(
							array(
								'id' => 'adjust',
								'name' => $isLangVn ? 'Điều chỉnh' : 'Adjust',
							),
							array(
								'id' => 'remove',
								'name' => $isLangVn ? 'Xóa' : 'Remove',
								'color' => '#e64d00',
							),
							array(
								'id' => 'cancel',
								'name' => $isLangVn ? 'Bỏ qua' : 'Dismiss',
								'color' => '#8c8c8c',
								'background' => '#f2f2f2',
							),
						),
					),
					'lowQualityMenuPopup' => array(
						'show' => false,
						'minWidth' => 500,
						'minHeight' => 500,
						'image' => null,
						'title' => $isLangVn ? 'Chất lượng hình ảnh thấp' : 'Low Image Quality',
						'description' => $isLangVn ? 'Bức ảnh này thực sự khá nhỏ. Nó có thể sẽ làm cho hỉnh ảnh mờ!' : 'This photo is actually pretty small. It will probably make a blurry tile!',
						'menus' => array(
							array(
								'id' => 'keep',
								'name' => $isLangVn ? 'Vẫn sử dụng' : 'Keep Anyway',
								'color' => '#e64d00',
							),
							array(
								'id' => 'remove',
								'name' => $isLangVn ? 'Xóa khỏi đơn hàng' : 'Remove From Order',
							),
						),
					),
					'checkoutPopup' => array(
						'show' => false,
						'title' => $isLangVn ? 'Thanh toán' : 'Check Out',
						'addAddress' => $isLangVn ? 'Thêm địa chỉ' : 'Add Address',
						'done' => $isLangVn ? 'Xong' : 'Done',
						'iconAddress' => base_url('assets/icon/addressIcon.svg'),
						'payment' => $isLangVn ? 'Thêm phương thức thanh toán' : 'Add Payment Method',
						'iconPayment' => base_url('assets/icon/creditCardIcon.svg'),
						'checkoutText' => $isLangVn ? 'Xác nhận đơn hàng' : 'Confirm order',
						'successNotify' => $isLangVn ? 'Đặt hàng thành công' : ' Order confirmed',
						'links' => array(
							'address' => array(
								'icon' => '',
								'name' => '',
								'fields' => array(
									array(
										'id' => 'fullname',
										'name' => $isLangVn ? 'Họ và tên *' : 'Full Name *',
										'value' => '',
										'placeholder' => '',
										'type' => 'text',
                                        'required' => true
									),
                                    array(
                                        'id' => 'phoneNumber',
                                        'name' => $isLangVn ? 'Số điện thoại *' : 'Phone Number',
                                        'value' => '',
                                        'placeholder' => '',
                                        'type' => 'text',
                                        'required' => true
                                    ),
                                    array(
                                        'id' => 'email',
                                        'name' => 'Email',
                                        'value' => '',
                                        'placeholder' => '',
                                        'type' => 'text',
                                        'required' => false
                                    ),
                                    /*array(
                                        'id' => 'province',
                                        'name' => 'Tỉnh',
                                        'value' => '',
                                        'placeholder' => '',
                                        'type' => 'select',
                                        'options' => array(),
                                        'required' => false
                                    ),
                                    array(
                                        'id' => 'district',
                                        'name' => 'Quận huyện',
                                        'value' => '',
                                        'placeholder' => '',
                                        'type' => 'select',
                                        'options' => array(),
                                        'required' => false
                                    ),
                                    array(
                                        'id' => 'ward',
                                        'name' => 'Phường xã',
                                        'value' => '',
                                        'placeholder' => '',
                                        'type' => 'select',
                                        'options' => array(),
                                        'required' => false
                                    ),*/
                                    array(
										'id' => 'address',
										'name' => $isLangVn ? 'Địa chỉ' : 'Shipping Address',
										'value' => '',
										'placeholder' => '',
										'type' => 'text',
                                        'required' => true
									),
								),
							),
							'payment' => array(
								'icon' => '',
								'name' => '',
							),
						),
					),
				),
				'headerMenuPopup' => array(
					'menus' => array() /*array(
						array(
							'id' => 'questions',
							'name' => $isLangVn ? 'Câu hỏi thường gặp' : 'Frequent Questions',
						),
						array(
							'id' => 'chat',
							'name' => $isLangVn ? 'Nói với chúng tôi' : 'Talk to Us',
						),
						array(
							'id' => 'promotion',
							'name' => $isLangVn ? 'Thêm mã khuyến mãi' : 'Add Promo Code',
						),
						array(
							'id' => 'gift',
							'name' => $isLangVn ? 'Thẻ quà tặng' : 'Gift Card',
						),
					)*/,
					'questions' => array(
						'title' => $isLangVn ? 'Câu hỏi thường gặp' : 'Frequent Questions',
						'contents' => $arrQuestion,
					),
                    'promotion' => array(
                        'done' => $isLangVn ? 'Xong' : 'Done',
                        'title' => $isLangVn ? 'Thêm mã khuyến mãi' : 'Add Promo Code',
                        'label' => $isLangVn ? 'Mã của bạn' : 'Your Code',
                        'inputPlaceholder' => $isLangVn ? 'Nhập mã...' : 'Code...',
                    ),
				),
				'gift' => array(
					'titleCard' => 'Thẻ quà tặng Decor22',
					'amount' => 'Tiles',
					'labelInput' => 'Ai tặng thẻ Decor22 cho bạn:',
					'placeholderInput' => 'Email',
					'imageCards' => base_url('assets/icon/card-en.png'),
					'shipIconUrl' => base_url('assets/icon/checkIcon.svg'),
					'shipText' => 'Bao gồm vận chuyển',
					'submitButton' => 'Mua',
					'howItWork' => 'Làm thế nào nó hoạt động?',
					'contactUs' => 'Mọi thắc mắc?<span class=\"contact-us-button\">Liên hệ</span>',
					'options' => array(
						array(
							'id' => 'gift3',
							'number' => 3,
							'title' => '3 cái',
							'price' => '300,000 VNĐ',
							'selected' => true,
						),
						array(
							'id' => 'gift6',
							'number' => 6,
							'title' => '6 cái',
							'price' => '600,000 VNĐ',
						),
						array(
							'id' => 'gift8',
							'number' => 8,
							'title' => '8 cái',
							'price' => '800,000 VNĐ',
						),
						array(
							'id' => 'gift12',
							'number' => 12,
							'title' => '12 cái',
							'price' => '1,200,000 VNĐ',
						),
					),
				),

			);
			echo json_encode($listConfigs);
		}
		catch (Exception $ex) {
			echo json_encode(array('success' => false));
		}
	}

	public function province(){
	    $this->openAllCors();
        echo json_encode(array());die();
        $fileName = FCPATH.'assets/tmp/province.txt';
	    if(file_exists($fileName)) echo file_get_contents($fileName);
	    else{
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards'));
            $listProvinces = $this->Mprovinces->getList('ProvinceId,ProvinceName');
            $listDistricts = $this->Mdistricts->getList(0, 'ProvinceId,DistrictId,DistrictName');
            $listWards = $this->Mwards->getList(0, 'DistrictId,WardId,WardName');
            $data = json_encode(array(
                'provinces' => $listProvinces,
                'districts' => $listDistricts,
                'wards' => $listWards
            ));
            echo $data;
            $this->load->helper('file');
            write_file($fileName, $data);
        }
    }

	public function createSession(){
		try { 
			$this->openAllCors();
            $datas = json_decode(file_get_contents('php://input'), true);
            $sessionId = $datas['sessionId'];
			if(!empty($sessionId)) {
                $this->loadModel(array('Morders', 'Merrors'));
                $orderStatusId = $this->Morders->getFieldValue(array('SessionId' => $sessionId), 'OrderStatusId', 0);
                if($orderStatusId == 0){
                    $refURL = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
                    $this->Morders->save(array('SessionId' => $sessionId, 'CustomerId' => 0, 'OrderStatusId' => 1, 'RefURL' => $refURL, 'CrDateTime' => getCurentDateTime()), 0, array('OrderDate', 'UpdateUserId', 'UpdateDateTime'));
                }
                elseif($orderStatusId > 1) $this->Merrors->save(array('ErrorDesc' => 'Trùng SessionId = ' . $sessionId, 'CrDateTime' => getCurentDateTime()));
                echo json_encode(array('success' => true));
			}
			else echo json_encode(array('success' => false));
		}
		catch (Exception $ex) {
			echo json_encode(array('success' => false));
		}
	}

	public function imageUpload($sessionId = ''){
		try { 
			$this->openAllCors();
			if(isset($_FILES['fileToUpload'])) {
				$file = $_FILES['fileToUpload']['tmp_name'];
				$uploadfile = file_get_contents($file);
				$ext = getimagesize($file);
				$nameFile1 = uniqid('decor22', true) . image_type_to_extension($ext[2]);
				$this->loadModel(array('Morderphotos', 'Morders'));
				$orderId = $this->Morders->getFieldValue(array('SessionId' => $sessionId, 'OrderStatusId > ' => 0), 'OrderId', 0);
				if($orderId > 0) {
                    $dir = IMAGE_PATH . $orderId . '/';
                    @mkdir($dir, 0777, true);
                    //@system("/bin/chown -R nginx:nginx ".$dir);
                    $nameFile = $dir . $nameFile1;
                    file_put_contents($nameFile, $uploadfile);
                    $this->correctImageOrientation($nameFile);
                    require_once APPPATH . "/libraries/imageCrop.php";
                    $imageCrop = new ImageCrop($nameFile, '', false);
                    $size = $imageCrop->getImageSize();
					$postData = array(
						'OrderId' => $orderId,
						'Width' => $size['width'],
						'Height' => $size['height'],
						'OriginalImage' => $orderId . '/'.$nameFile1,
						'EditedImage' => '',
						'PhotoStyleId' => 3,
						'Price' => $this->Mconfigs->getConfigValue('PRICE', 0),
						'StatusId' => 1
					);
					$flag = $this->Morderphotos->save($postData);
					if ($flag) {
						echo json_encode(array(
							'success' => true,
							'size' => $size,
							'id' => $flag,
							'path' => base_url($nameFile)
						));
					}
					else echo json_encode(array('success' => false));
				}
				else echo json_encode(array('success' => false));
			}
			else echo json_encode(array('success' => false));
		}
		catch (Exception $ex) {
			echo json_encode(array('success' => false));
		}
	}

    private function correctImageOrientation($filename) {
        if (function_exists('exif_read_data')) {
            $exif = @exif_read_data($filename);
            if($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                if($orientation != 1){
                    $img = imagecreatefromjpeg($filename);
                    $deg = 0;
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                    if ($deg) {
                        $img = imagerotate($img, $deg, 0);
                        // then rewrite the rotated image back to the disk as $filename
                        imagejpeg($img, $filename, 100);
                    }
                }
            }
        }
    }

    public function deleteImage($orderPhotoId = 0){
        $this->openAllCors();
        $flag = false;
        if($orderPhotoId > 0){
            $this->loadModel(array('Morderphotos'));
            $flag = $this->Morderphotos->changeStatus(0, $orderPhotoId);
        }
        echo json_encode(array('success' => $flag));
    }

	public function addressAdd($sessionId = ''){
        try {
            $this->openAllCors();
            if(!empty($sessionId)) {
                $this->loadModel(array('Mcustomers', 'Morders'));
                //$customerId = $this->Mcustomers->getFieldValue(array('SessionId' => $sessionId, 'StatusId' => STATUS_ACTIVED), 'CustomerId', 0);
                //if ($customerId > 0) {
                    $datas = @json_decode(file_get_contents('php://input'), true);
                    if(!is_array($datas)) $datas = array();
                    $postData = array();
                    foreach ($datas as $k => $d) {
                        switch ($d['id']) {
                            case 'fullname':
                                $postData['FullName'] = $d['value'];
                                break;
                            case 'phoneNumber':
                                $postData['PhoneNumber'] = $d['value'];
                                break;
                            case 'email':
                                $postData['Email'] = $d['value'];
                                break;
                            case 'province':
                                $postData['ProvinceId'] = $d['value'];
                                break;
                            case 'district':
                                $postData['DistrictId'] = $d['value'];
                                break;
                            case 'ward':
                                $postData['WardId'] = $d['value'];
                                break;
                            case 'address':
                                $postData['Address'] = $d['value'];
                                break;
                            default:
                        }
                    }
                    if (!empty($postData)){
                        $postData['StatusId'] = STATUS_ACTIVED;
                        $customerId = $this->Mcustomers->update($postData, $sessionId);
                        if ($customerId > 0) echo json_encode(array('success' => true));
                        else echo json_encode(array('success' => false));
                    }
                    else echo json_encode(array('success' => false));
                //} else echo json_encode(array('success' => false));
            }
            else echo json_encode(array('success' => false));
        }
        catch (Exception $ex) {
            echo json_encode(array('success' => false));
        }
	}

	public function promotionApply($sessionId = ''){
		try {
			$this->openAllCors();
			$flag = false;
			$promotionCode = $this->input->post('promotionCode');
			if(!empty($sessionId) && !empty($promotionCode)) {
				$this->loadModel(array('Mpromotions'));
				$flag = $this->Mpromotions->checkPromotionCode($sessionId, $promotionCode);
			}
			echo json_encode(array('success' => $flag));
		}
		catch (Exception $ex) {
			echo json_encode(array('success' => false));
		}
	}

	public function getCheckout($sessionId = ''){
		try {
			$this->openAllCors();
            //log_message('error', json_encode($_SERVER));
			$ids = isset($_GET['ids']) ? explode(',', trim($_GET['ids'])) : '';
			if(!empty($sessionId) && !empty($ids)){
                $this->loadModel(array('Mpromotions', 'Morders', 'Morderphotos'));
                $order = $this->Morders->getBy(array('SessionId' => $sessionId, 'OrderStatusId >' => 0), true);
                if($order) {
                    $isLangVn = true;
                    if(isset($_SERVER['HTTP_LANGCODE'])) $isLangVn = !($_SERVER['HTTP_LANGCODE'] == 'en');
                    $this->Morderphotos->removeCheck($ids, $order['OrderId']);
                    $orderPhoto = $this->Morderphotos->getTotalPhoto($order['OrderId']);
                    $promotion = $this->Mpromotions->getPercent($order);
                    $promotionValue = 0;
                    if ($promotion) {
                        if ($promotion[0]['OrderPaidVN'] > 0) {
                            $promotionValue = $promotion[0]['OrderPaidVN'];
                        }
                        else {
                            $promotionValue = ($orderPhoto['Price'] * $promotion[0]['OrderPercent']) / 100;
                        }
                        $priceCheckout = $orderPhoto['Price'] - $promotionValue;
                    }
                    else {
                        $priceCheckout = $orderPhoto['Price'];
                    }
                    $shipPrice = 0;
                    $configSites =  $this->Mconfigs->getListMap();
                    if($orderPhoto['TotalPhoto'] < intval($configSites['QUANTITY'])){
                        $priceCheckout = $priceCheckout + intval($configSites['PRICE_SHIPPING']);
                        $shipPrice = $configSites['PRICE_SHIPPING'];
                    }
                    $discount = 0;
                    $perPrice = $configSites['PRICE'];
                    $totalPrice = $perPrice * $orderPhoto['TotalPhoto'];
                    $data = array(
                        array(
                            "name" => $orderPhoto['TotalPhoto'] . ($isLangVn ? ' cái' : ' tile'),
                            "value" => priceFormat($totalPrice) . ($isLangVn ? ' VNĐ' : ' VND'),
                        ),
                    );
                    if($orderPhoto['TotalPhoto'] > 6){
                        if($orderPhoto['TotalPhoto'] > 100) $discount = $totalPrice - 80000 * $orderPhoto['TotalPhoto'];
                        elseif($orderPhoto['TotalPhoto'] > 30) $discount = $totalPrice - 85000 * $orderPhoto['TotalPhoto'];
                        elseif($orderPhoto['TotalPhoto'] > 15) $discount = $totalPrice - 90000 * $orderPhoto['TotalPhoto'];
                        else $discount = $totalPrice - 95000 * $orderPhoto['TotalPhoto'];
                        $data[] = array(
                            "name" => $isLangVn ? "Giảm giá" : "Discount",
                            "value" => '-'.priceFormat($discount) . ($isLangVn ? ' VNĐ' : ' VND')
                        );
                        $priceCheckout -= $discount;
                    }
                    $data[] = array(
                        "name" => $isLangVn ? "Vận chuyển" : "Shipping",
                        "value" => $shipPrice > 0 ? priceFormat($shipPrice) . ($isLangVn ? ' VNĐ' : ' VND') : ($isLangVn ? 'Miễn phí' : 'Free'),
                    );
                    $data[] = array(
                        "name" => $isLangVn ? "Tổng cộng" : "Total",
                        "value" => priceFormat($priceCheckout) . ($isLangVn ? ' VNĐ' : ' VND'),
                    );
                    $dataReturn = array(
                        'success' => true,
                        'orderCode' => $this->Morders->genOrderCode($order['OrderId']),
                        'products' => $orderPhoto['TotalPhoto'] . ($isLangVn ? ' ảnh' : ' tile'),
                        'price' => priceFormat($perPrice),
                        'priceUnit' => ($isLangVn ? 'VNĐ' : 'VND'),
                        'promotionValue' => $promotionValue,
                        'promotionUnit' => ($isLangVn ? 'VNĐ' : 'VND'),
                        'priceCheckout' => priceFormat($priceCheckout),
                        'shipPrice' => priceFormat($shipPrice),
                        'canCheckout' => true,
                        'data' => $data,
                    );
                    $this->Morders->save(array(
                        'PaidVN' => $priceCheckout,
                        'PromotionCost' => $promotionValue,
                        'Discount' => $discount,
                        'ShipCost' => $shipPrice,
                        'PhotoCount' => $orderPhoto['TotalPhoto']
                    ), $order['OrderId']);
                    echo json_encode($dataReturn);
                }
                else echo json_encode(array('success' => false));
			}
			else echo json_encode(array('success' => false));
		}
		catch (Exception $ex) {
			echo json_encode(array('success' => false));
		}
	}

    public function checkout($sessionId = ''){
        try {
            $this->openAllCors();
            $datas = json_decode(file_get_contents('php://input'), true);
            if(isset($datas['imageData']) && !empty($sessionId)) {
                $photoUpdates = array();
                foreach($datas['imageData'] as $id){
                    switch($id['frame']){
                        case 'clean':
                            $photoStyleId = 1;
                            break;
                        case 'ever':
                            $photoStyleId = 2;
                            break;
                        case 'bold':
                            $photoStyleId = 3;
                            break;
                        case 'classic':
                            $photoStyleId = 4;
                            break;
                        default:
                            $photoStyleId = 3;
                            break;
                    }
                    $photoUpdates[] = array(
                        'EditedImage' => '',
                        'PhotoStyleId' => $photoStyleId,
                        'CropData' => $id['cropData'],
                        'StatusId' => 3,//chua process image
                        'OrderPhotoId' => $id['imageId']
                    );
                }
                $this->load->model('Morders');
                $flag = $this->Morders->updateCheckout($photoUpdates, $sessionId);
                echo json_encode(array('success' => $flag));
            }
            else echo json_encode(array('success' => false));
        }
        catch (Exception $ex) {
            echo json_encode(array('success' => false));
        }
    }

	public function news($sessionId = '', $page = 1){
        $this->openAllCors();
        $this->loadModel(array('Marticles'));
        $postData = array();
        $rowCount = $this->Marticles->getCount($postData);
        $listArticles = array();
        $perPage = DEFAULT_LIMIT;
        if($rowCount > 0){
            $pageCount = ceil($rowCount / $perPage);
            if(!is_numeric($page) || $page < 1) $page = 1;
            $listArticles = $this->Marticles->search($postData, $perPage, $page);
            $datas = array('news' => array());
            for ($i=0; $i < count($listArticles) ; $i++) {
                $image = IMAGE_PATH.(empty($listArticles[$i]['ArticleImage']) ? NO_IMAGE : $listArticles[$i]['ArticleImage']);
                $datas['news'][] = array(
                    'id' => $listArticles[$i]['ArticleId'],
                    'slug' => $listArticles[$i]['ArticleSlug'],//$this->Mconstants->getUrl($listArticles[$i]['ArticleSlug'], $listArticles[$i]['ArticleId'], 4),
                    'title' => $listArticles[$i]['ArticleTitle'],
                    'description' => strip_tags($listArticles[$i]['ArticleContent']),
                    'image' => base_url().$image,
                );
            }

        }
        $datas['seo'] = array(
            "title"=> "News Detail",
            "metaTags" => array(
                array(
                    "name"=> "name",
                    "value"=> "keywords"
                ),
                array(
                    "name"=> "content",
                    "value"=> "Từ khóa"
                ),
                array(
                    "name"=> "name",
                    "value"=> "description"
                ),
                array(
                    "name"=> "content",
                    "value"=> "Mô tả ở đây"
                ),
            ),
        );
        $datas['nextText'] = 'Load more';
        $datas['page'] = $page;
        $datas['limit'] = $perPage;
        $datas['total'] = $rowCount;
        echo json_encode($datas);
	}

	public function newsDetailAndPage($sessionId = '', $slug = ''){
		try {
			$this->openAllCors();
			$this->loadModel(array('Marticles'));
			$article = $this->Marticles->getBy(array('ArticleSlug' => $slug, 'ArticleStatusId' => STATUS_ACTIVED));
			if($article){
				$article = $article[0];
				if($article['ArticleSlug'] == $slug) {
					$image = IMAGE_PATH.(empty($article['ArticleImage']) ? NO_IMAGE : $article['ArticleImage']);
					$data = array(
						'seo' => array(
							"title"=> "News Detail",
							"metaTags" => array(
								array(
									"name"=> "name",
									"value"=> "keywords"
								),
								array(
									"name"=> "content",
									"value"=> "Từ khóa"
								),
								array(
									"name"=> "name",
									"value"=> "description"
								),
								array(
									"name"=> "content",
									"value"=> "Mô tả ở đây"
								),
							),
						),
						'banner' => base_url().$image,
						'title' => $article['ArticleTitle'],
						'content' => strip_tags($article['ArticleContent']),
					);
					echo json_encode($data);
				}
				else echo json_encode(array('success' => false));
			}
			else echo json_encode(array('success' => false));
		}
		catch (Exception $ex) {
			echo json_encode(array('success' => false));
		}
	}

	public function imageTemplate($slug = ''){
        $this->openAllCors();
        if(!empty($slug)){
            $this->loadModel(array('Mtemplates', 'Mtemplateimages'));
            $templateId = $this->Mtemplates->getFieldValue(array('TemplateSlug' => $slug, 'StatusId' => STATUS_ACTIVED), 'TemplateId', 0);
            if($templateId > 0){
                $listImages = $this->Mtemplateimages->getBy(array('TemplateId' => $templateId));
                $data = array();
                foreach ($listImages as $img){
                    $data[] = array(
                        'id' => $img['TemplateImageId'],
                        'name' => '',
                        'path' => base_url(IMAGE_PATH.$img['ImagePath']),
                        'help' => true,
                        'size' => array(
                            'width' => $img['Width'],
                            'height' => $img['Height']
                        )
                    );
                }
                echo json_encode(array(
                    'success' => true,
                    'data' => $data
                ));
            }
            else echo json_encode(array('success' => false));
        }
        else echo json_encode(array('success' => false));
    }
}