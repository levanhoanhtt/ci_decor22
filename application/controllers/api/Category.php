<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function get($itemTypeId = 4){
		header('Content-Type: application/json');
		$this->load->model('Mcategories');
		$listCategories = $this->Mcategories->getListByItemType($itemTypeId, false, true);
		$arrData = array();
		for ($i=0; $i < count($listCategories); $i++) { 
			$arrData[] = array(
				'categoryId' => $listCategories[$i]['CategoryId'],
				'categoryName' => $listCategories[$i]['CategoryName'],
			);
		}
		echo json_encode($arrData);
	}
}