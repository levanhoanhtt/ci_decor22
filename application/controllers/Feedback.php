<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user, 'Danh sách Phản hồi');
        if($this->Mactions->checkAccess($data['listActions'], 'feedback')) {
            $this->load->model('Mfeedbacks');
            $postData = $this->arrayFromPost(array('FullName', 'PhoneNumber', 'Email'));
            $rowCount = $this->Mfeedbacks->getCount($postData);
            $data['listFeedbacks'] = array();
            if($rowCount > 0){
                $perPage = 20;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listFeedbacks'] = $this->Mfeedbacks->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('feedback/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function insert(){
        $postData = $this->arrayFromPost(array('FullName', 'PhoneNumber', 'Email', 'Address', 'Content'));
        if(!empty($postData['FullName']) && !empty($postData['PhoneNumber']) && !empty($postData['Content'])){
            $postData['IpAddress'] = $this->input->ip_address();
            $postData['UserAgent'] = $this->input->user_agent();
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mfeedbacks');
            $id = $this->Mfeedbacks->save($postData);
            if($id > 0) echo json_encode(array('code' => 1, 'message' => "Thank you for leaving a message, we will get back to you soon."));
            else echo json_encode(array('code' => 0, 'message' => 'An error occurred during the execution'));
        }
        else echo json_encode(array('code' => -1, 'message' => 'An error occurred during the execution'));
    }
}